CREATE SCHEMA IF NOT EXISTS `AuctorDb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `AuctorDb` ;

-- -----------------------------------------------------
-- Table `AuctorDb`.`callog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`callog` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`callog` (
  `Time` DATETIME NOT NULL ,
  `Caller` SMALLINT NULL ,
  `Callee` TINYINT NULL ,
  `Source Trunk` CHAR NULL ,
  `Destination Trunk` CHAR NULL ,
  `Duration` TIME NULL ,
  `Billing Duration` TIME NULL ,
  `Disposition` CHAR NULL ,
  `Communication Type` CHAR NULL ,
  PRIMARY KEY (`Time`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`aa_report`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`aa_report` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`aa_report` (
  `Report_From` DATETIME NOT NULL ,
  `Report_To` DATETIME NULL ,
  `Start_Data` DATETIME NULL ,
  `End_Data` DATETIME NULL ,
  `Total_Number_Of_Calls_For_All_Trees` INT NULL ,
  `Tree_Number_Of_Calls` INT NULL ,
  `Number_Of_Calls` INT NULL ,
  `Mean Duration` FLOAT NULL ,
  `Tree_Node_Activity` SMALLINT NULL ,
  `Node` SMALLINT NULL ,
  `Count` SMALLINT NULL ,
  `Percentage` FLOAT NULL ,
  `Hangup` SMALLINT NULL ,
  `Hangup_Per_Node` SMALLINT NULL ,
  `Cause` SMALLINT NULL ,
  `Number_Of_Calls` INT NULL ,
  `Tree_Location_Of_Hangup` INT NULL ,
  `Tree_Node` INT NULL ,
  `Tree_Count` INT NULL ,
  `ACD_` VARCHAR(45) NULL ,
  PRIMARY KEY (`Report_From`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Trunk_Group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Trunk_Group` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Trunk_Group` (
  `Filter` INT NOT NULL COMMENT '	' ,
  `System` INT NULL ,
  `Trunk_Group` VARCHAR(45) NULL ,
  `Duration` DATETIME NULL ,
  `Number_Calls` INT NULL ,
  `Calls_In` INT NULL ,
  `Calls_Out` INT NULL ,
  `Total_Duration` TIME NULL ,
  `Total_Number_Calls` INT NULL ,
  PRIMARY KEY (`Filter`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Llamadas_Perdidas_Detallado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Llamadas_Perdidas_Detallado` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Llamadas_Perdidas_Detallado` (
  `Filter` VARCHAR(45) NOT NULL ,
  `Name` VARCHAR(45) NULL ,
  `First_Name` VARCHAR(45) NULL ,
  `Extension` INT NULL ,
  `Date_Time` DATETIME NULL ,
  `Cost_Center` VARCHAR(45) NULL ,
  `Duration` TIME NULL 
   )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Llamadas_Perdidas_Totales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Llamadas_Perdidas_Totales` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Llamadas_Perdidas_Totales` (
  `Filter` CHAR NOT NULL ,
  `Level` INT NULL ,
  `Name` VARCHAR(45) NULL ,
  `Extension` SMALLINT NULL ,
  `Number_Calls` INT NULL ,
  `Duration` TIME NULL ,
  `Cost_Center` CHAR NULL ,
  PRIMARY KEY (`Filter`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Llamadas_Salientes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Llamadas_Salientes` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Llamadas_Salientes` (
  `Filter` CHAR NOT NULL ,
  `Cost Center` VARCHAR(45) NULL ,
  `Name` VARCHAR(45) NULL ,
  `First_Name` VARCHAR(45) NULL ,
  `Extension` SMALLINT NULL ,
  `Date_Hour` DATETIME NULL ,
  `Callee` INT NULL ,
  `Total` VARCHAR(30) NULL ,
  PRIMARY KEY (`Filter`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Llamadas_Entrantes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Llamadas_Entrantes` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Llamadas_Entrantes` (
  `Filter` CHAR NOT NULL ,
  `Cost_Center` CHAR NULL ,
  `Name` VARCHAR(45) NULL ,
  `First_Name` VARCHAR(45) NULL ,
  `Extension` INT NULL ,
  `Date_Time` DATETIME NULL ,
  `Callee` INT NULL ,
  `Duration` TIME NULL ,
  PRIMARY KEY (`Filter`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Llamadas_Internas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Llamadas_Internas` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Llamadas_Internas` (
  `Filter` CHAR NOT NULL ,
  `Cost_Center` CHAR NULL ,
  `Name` VARCHAR(45) NULL ,
  `Extension` INT NULL ,
  `Date_Time` DATETIME NULL ,
  `Callee` INT NULL ,
  `Duration` TIME NULL ,
  PRIMARY KEY (`Filter`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Diario_Agente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Diario_Agente` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Diario_Agente` (
  `Agente_Numero` INT NOT NULL ,
  `Fecha` DATE NULL ,
  `Rotaciones_Sobre_Timbre` INT NULL ,
  `Llamadas_Hacia_Agente_Capturadas` INT NULL ,
  `Capturas_Agente` INT NULL ,
  `Salidas_Locales` INT NULL ,
  `Salidas_Externas` INT NULL ,
  `Timbres_ACD` INT NULL ,
  `Llamadas_Con_Peticion_Ayuda` INT NULL ,
  `Llegadas_Locales_No_ACD` INT NULL ,
  `Llegadas_Externas_No_ACD_Directas` INT NULL ,
  `Llegadas_Externas_No_ACD_Por_Transferencia` INT NULL ,
  `Llamadas_ACD_Atendidas_Sin_Codigo_Transaccion` INT NULL ,
  `Llamadas_ACD_Atendidas_Con_Codigo_Transaccion` INT NULL ,
  `Llamadas_ACD_Atendidas_Liberadas_Antes_Umbral` INT NULL ,
  `Llegadas_Externas_No_ACD_Atendidas` INT NULL ,
  `Llegadas_Externas_No_ACD_Atendidas_Liberadas_Antes_Umbral` INT NULL ,
  `Llegadas_ACD_Salientes` INT NULL ,
  `Llegadas_ACD_Salientes_Respondidas` INT NULL ,
  `Llamadas_Sobre_Wrap_Up` INT NULL ,
  `Tiempo_Total_Timbre_Llamadas_ACD_Atendidas` TIME NULL ,
  `Tiempo_Medio_Timbre_Llamadas_ACD_Atendidas` TIME NULL ,
  `Timbre_Llamadas_Entrantes_Externas_No_ACD_Atendidas` INT NULL ,
  `Timbre_Llamadas_Entrantes_Externas_No_ACD_Atendidas` INT NULL ,
  `Tiempo_Timbre_ACD` TIME NULL ,
  `Tiempo_Medio_Timbre_ACD` TIME NULL ,
  `Tiempo_Timbre_Llegada_Externa_No_ACD` TIME NULL ,
  `Tiempo_Medio_Timbre_Llegada_Externa_No_ACD` TIME NULL ,
  `Tiempo_Timbre` TIME NULL ,
  `Tiempo_Medio_Timbre` TIME NULL ,
  `Tiempo_Conversacion_ACD` TIME NULL ,
  `Tiempo_Medio_Conversacion_ACD` TIME NULL ,
  `Tiempo_Total_Wrap_Up_Todas_Llamadas_Atendidas` TIME NULL ,
  `Tiempo_Conversacion_Salidas_Locales` TIME NULL ,
  `Tiempo_Medio_Coversacion_Llamadas_Salientes_Internas` TIME NULL ,
  `Tiempo_Coversacion_Salidas_Externas` TIME NULL ,
  `Tiempo_Medio_Conversacion_Llamadas_Salientes_Externas` TIME NULL ,
  `Tiempo_Conversacion_Llegadas_Locales_No_ACD` TIME NULL ,
  `Media_Conversacion_Llamadas_Entrantes_Internas_No_ACD` TIME NULL ,
  `Tiempo_Conversacion_Llegadas_Externas_No_ACD` TIME NULL ,
  `Tiempo_Medio_Conversacion_Llegadas_Externas_No_ACD` TIME NULL ,
  `Tiempo_Total_Tratamiento_Llamadas_ACD_Salientes` TIME NULL ,
  `Tiempo_Medio_Tratamiento_Llamadas_ACD_Salientes` TIME NULL ,
  `Tiempo_Total_Conversacion_Llamadas_ACD_Salientes` TIME NULL ,
  `Tiempo_Medio_Conversacion_Llamadas_ACD_Salientes` TIME NULL ,
  `Tiempo_Total_Fase_Transaccion_Llamadas_ACD_Salientes` TIME NULL ,
  `Tiempo_Medio_Fase_Transaccion_Llamadas_ACD_Salientes` TIME NULL ,
  `Tiempo_Total_Wrap_Up_Llamadas_ACD_Salientes` TIME NULL ,
  `Tiempo_Medio_Wrap_Up_Llamadas_ACD_Salientes` TIME NULL ,
  `Tiempo_Total_Fase_Pausa` TIME NULL ,
  `Tiempo_Medio_Fase_Pausa` TIME NULL ,
  `Tiempo_Wrap_Up_Reposo` TIME NULL ,
  `Duracion_Total_Conversacion_Wrap_Up` TIME NULL ,
  `Duracion_Total_Ocupado_Wrap_Up` TIME NULL ,
  `Tiempo_Pasado_Estado_Inaccesible` TIME NULL ,
  `St_Agente_No_Conectado` FLOAT NULL ,
  `St_Agente_Conectado_No_Asignado` FLOAT NULL ,
  `St_Agente_Conectado_Asignado_No_Retirado` FLOAT NULL ,
  `St_Agente_Tiempo_Retirado` TIME NULL ,
  `St_Agente_Retirado_Causa_1` FLOAT NULL ,
  `St_Agente_Retirado_Causa_2` FLOAT NULL ,
  `St_Agente_Retirado_Causa_3` FLOAT NULL ,
  `St_Agente_Retirado_Causa_4` FLOAT NULL ,
  `St_Agente_Retirado_Causa_5` FLOAT NULL ,
  `St_Agente_Retirado_Causa_6` FLOAT NULL ,
  `St_Agente_Retirado_Causa_7` FLOAT NULL ,
  `St_Agente_Retirado_Causa_8` FLOAT NULL ,
  `St_Agente_Retirado_Causa_9` FLOAT NULL ,
  `Pilotos_Tratados` INT NULL ,
  PRIMARY KEY (`Agente_Numero`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Diario_Sesion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Diario_Sesion` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Diario_Sesion` (
  `Nombre_Agente` VARCHAR(45) NOT NULL ,
  `Numero_Directorio` INT NULL ,
  `Dia` DATE NULL ,
  `Hora_Logon` TIME NULL ,
  `Hora_Logout` TIME NULL ,
  `Duracion_Logon` TIME NULL ,
  `Duracion_Total_Retiradas` TIME NULL ,
  `Duracion_Total_Wrap_Up_Manual` TIME NULL ,
  `Llamadas_No_ACD` INT NULL ,
  `Duracion_Total_Llamadas_No_ACD` TIME NULL ,
  `Nombre_Piloto` VARCHAR(45) NULL ,
  `Directorio_Piloto` INT NULL ,
  `Llamadas_Inbound_ACD_Respondidas` INT NULL ,
  `Duracion_Total_Comunicacion_Llamadas_Inbound_Respondidas` TIME NULL ,
  `Duracion_Total_Conversacion_Llamadas_Inbound` TIME NULL ,
  `Duracion_Total_Wrap_Up_Llamadas_Inbound` TIME NULL ,
  `Llamadas_Outbound_ACD_Respondidas` INT NULL ,
  `Duracion_Total_Comunicacion_Llamadas_Outbound_Respondidas` TIME NULL ,
  `Duracion_Total_Conversacion_Llamadas_Outbound_ACD` TIME NULL ,
  `Duracion_Total_Wrap_Up_Llamadas_Outbound` TIME NULL ,
  PRIMARY KEY (`Nombre_Agente`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AuctorDb`.`Diario_Piloto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AuctorDb`.`Diario_Piloto` ;

CREATE  TABLE IF NOT EXISTS `AuctorDb`.`Diario_Piloto` (
  `Codigo_Piloto` INT NOT NULL ,
  `Fecha` DATE NULL ,
  `Llamadas_Recibidas_Estado_Abierto` INT NULL ,
  `Llamadas_Recibidas_Estado_Bloqueado` INT NULL ,
  `Llamadas_Recibidas_Estado_Desvio_General` INT NULL ,
  `Llamadas_Recibidas_Por_Transferencia` INT NULL ,
  `Llamadas_Recibidas_Ayuda_Mutua` INT NULL ,
  `Llamadas_Maximas_Simultaneas` INT NULL ,
  `Desbordamiento_Durante_Retencion` INT NULL ,
  `Desbordamiento_Durante_Timbre` INT NULL ,
  `Llamadas_Atendidas_Sin_Espera` INT NULL ,
  `Llamadas_Atendidas_Con_Espera` INT NULL ,
  `Llamadas_Ayuda_Mutua` INT NULL ,
  `Llamadas_Redirigidas_Fuera_ACD` INT NULL ,
  `Llamadas_Disuadidas` INT NULL ,
  `Llamadas_Disuadidas_Despues_Intento_Ayuda` INT NULL ,
  `Llamadas_Procesadas_GT_Tipo_Guia` INT NULL ,
  `Llamadas_Enviadas_GT_Remoto` INT NULL ,
  `Llamadas_Rechazadas_Por_Falta_Recursos` INT NULL ,
  `Llamadas_Atendidas` INT NULL ,
  `Llamadas_Atendidas_Segun_GS` INT NULL ,
  `Llamadas_Atendidas_Liberadas_Antes_Umbral` INT NULL ,
  `Sin_Codigo_Transaccion` INT NULL ,
  `Con_Codigo_Transaccion` INT NULL ,
  `Redistribucion_ACD` INT NULL ,
  `St_Piloto_Llamadas_Atendidas_Antes_5_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Llamadas_Atendidas_Antes_5_Segundos` FLOAT NULL ,
  `St_Piloto_Llamadas_Atendidas_Antes_15_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Llamadas_Atendidas_Antes_15_Segundos` FLOAT NULL ,
  `St_Piloto_Llamadas_Atendidas_Antes_30_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Llamadas_Atendidas_Antes_30_Segundos` FLOAT NULL ,
  `St_Piloto_Llamadas_Atendidas_Antes_60_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Llamadas_Atendidas_Antes_60_Segundos` FLOAT NULL ,
  `St_Piloto_Llamadas_Atendidas_Despues_60_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Llamadas_Atendidas_Despues_60_Segundos` FLOAT NULL ,
  `Abandonos_Guia_Presentacion` INT NULL ,
  `Abandonos_1_Guia_Espera` INT NULL ,
  `Abandonos_2_Guia_Espera` INT NULL ,
  `Abandonos_3_Guia_Espera` INT NULL ,
  `Abandonos_4_Guia_Espera` INT NULL ,
  `Abandonos_5_Guia_Espera` INT NULL ,
  `Abandono_6_Guia_Espera` INT NULL ,
  `Abandonos_Timbre_Agente` INT NULL ,
  `Abandonos_Guia_Desvio_General` INT NULL ,
  `Abandonos_Guia_Bloqueo` INT NULL ,
  `Abandonos_Llamadas_Directas` INT NULL ,
  `Abandonos_Llamadas_Directas_Mientras_Espera_Agente` INT NULL ,
  `Total_Abandonos` INT NULL ,
  `St_Piloto_Abandonos_Antes_5_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Abandonos_Antes_5_Segundos` FLOAT NULL ,
  `St_Piloto_Abandonos_Antes_15_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Abandonos_Antes_15_Segundos` FLOAT NULL ,
  `St_Piloto_Abandonos_Antes_30_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Abandonos_Antes_30_Segundos` FLOAT NULL ,
  `St_Piloto_Abandonos_Antes_60_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Abandonos_Antes_60_Segundos` FLOAT NULL ,
  `St_Piloto_Abandonos_Despues_60_Segundos` INT NULL ,
  `St_Piloto_Porcentaje_Abandonos_Despues_60_Segundos` FLOAT NULL ,
  `Tiempo_Total_Tratamiento_Llamada` TIME NULL ,
  `Tiempo_Medio_Tratamiento_Llamada` TIME NULL ,
  `Tiempo_Total_Escucha_Guia_Bienvenida` TIME NULL ,
  `Tiempo_Medio_Escucha_Guia_Bienvenida` TIME NULL ,
  `Tiempo_Total_Antes_Puesta_En_Cola` TIME NULL ,
  `Tiempo_Total_Espera` TIME NULL ,
  `Tiempo_Medio_Espera` TIME NULL ,
  `Duracion_Total_Espera_Llamadas_Abandonadas` TIME NULL ,
  `Duracion_Media_Espera_Llamadas_Abandonadas` TIME NULL ,
  `Tiempo_Total_Timbre` TIME NULL ,
  `Tiempo_Medio_Timbre` TIME NULL ,
  `Tiempo_Total_Conversacion` TIME NULL ,
  `Tiempo_Medio_Conversacion` TIME NULL ,
  `Tiempo_Total_Llamadas_Consulta` TIME NULL ,
  `Tiempo_Medio_Llamadas_Consulta` TIME NULL ,
  `Tiempo_Total_Wrap_Up` TIME NULL ,
  `Tiempo_Medio_Wrap_Up` TIME NULL ,
  `Tiempo_Espera_Maxima` TIME NULL ,
  `Llamadas_Contectadas_IVR_Mientras_Espera` INT NULL ,
  `Llamadas_Rechazadas_IVR` INT NULL ,
  `Tiempo_Total_Llamadas_Conectadas_IVR` TIME NULL ,
  `Nivel_Atencion` FLOAT NULL ,
  `Eficiencia` FLOAT NULL ,
  `St_Piloto_Porcentaje_Servicio` FLOAT NULL ,
  `St_Piloto_Porcentaje_Desvio_General` FLOAT NULL ,
  `St_Piloto_Porcentaje_Bloqueado` FLOAT NULL ,
  PRIMARY KEY (`Codigo_Piloto`) )
ENGINE = InnoDB;