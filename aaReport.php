<?php
require('db.php');
?>
<!DOCTYPE html>
<html>
<head>
	<?php
	session_start();
	?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

	<!-- Page title -->
	<title>Auctor | CTI Monitor</title>

	<!-- Vendor styles -->
	<link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
	<link rel="stylesheet" href="vendor/animate.css/animate.css"/>
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>
	<link rel="stylesheet" href="vendor/datatables/datatables.min.css"/>

	<!-- App styles -->
	<link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
	<link rel="stylesheet" href="styles/pe-icons/helper.css"/>
	<link rel="stylesheet" href="styles/stroke-icons/style.css"/>
	<link rel="stylesheet" href="styles/style.css">
</head>
<body>

	<!-- Wrapper-->
	<div class="wrapper">
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    $var = basename(__FILE__);
    if ($var == 'ivr.php') {
    	echo 'class = "active"';
    }
    
    ?>
    <!-- End navigation-->

    <!-- Main content-->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-12">
    				<div class="view-header">
    					<div class="header-icon">
    						<i class="pe page-header-icon pe-7s-albums"></i>
    					</div>
    					<div class="header-title">
    						<h3>Datos de Uso - I.V.R.</h3>
    						<small>
    							Registro de Actividad | Auctor CTI Monitor   
    						</small>
    					</div>
    				</div>
    				<hr>
    			</div>
    		</div>

            <div class="col-lg-4">
                <form action="uploadhtml.php" method="post" enctype="multipart/form-data">
                    <label class="btn btn-accent" for="fileupload">
                        <input name="fileupload" id="fileupload" type="file" style="display:none;"
                        onchange="$('#upload-file-info2').html($(this).val());">
                        <span id="upload-file-info2">Seleccionar Archivo</span>
                    </label>
                    <input type="submit" class="btn btn-w-md btn-success" value="Cargar">
                </form>
            </div>

            <div class="row">
              <div class="col-md-12">
               <div class="panel panel-filled">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
              <div class="table-responsive">

                  <table id="tableExample3" class="table table-striped table-hover">
                   <thead>
                    <tr align='center'>
                     <th>Fecha Inicio</th>
                     <th>Hora Inicio</th>
                     <th>Fecha Fin</th>
                     <th>Hora Fin</th>
                     <th>Info Rep Datos Fecha Inc</th>
                     <th>Info Rep Datos Hora Inc</th>
                     <th>Ref Inici</th>
                     <th>Info Rep Datos Fecha Fin</th>
                     <th>Info Rep Datos Hora Fin</th>
                     <th>Ref Fin</th>
                 </tr>
             </thead>
             <tbody>
                <?php
                $i=0;
                $consulta = "SELECT * FROM tb_informacion_rep";
                $resultado = $mysqli->query($consulta);
                while ($fila = $resultado->fetch_row()) {
                    
                   echo "<tr align='center'>";
                   echo "<td>$fila[1]";
                   echo "<td>$fila[2]";
                   echo "<td>$fila[3]";
                   echo "<td>$fila[4]";
                   echo "<td>$fila[5]";
                   echo "<td>$fila[6]";
                   echo "<td>$fila[7]";
                   echo "<td>$fila[8]";                                         
                   echo "<td>$fila[9]";                                         
                   echo "<td>$fila[10]";                                         
                   echo "</tr>";
                 
              }
              ?>
          </tbody>
      </table>
  </div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="vendor/pacejs/pace.min.js"></script>
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/datatables/datatables.min.js"></script>
<script src="vendor/chart.js/dist/Chart.min.js"></script>


<!-- App scripts -->
<script src="scripts/luna.js"></script>


<script>
	$(document).ready(function () {
      open();
    
		$('#tableExample3').DataTable({
			dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
			"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
			buttons: [

            {extend: 'csv',title: 'aaReport', className: 'btn-sm'},
            {extend: 'excelHtml5',title: 'aaReport', className: 'btn-sm'},            
            {extend: 'pdf', title: 'aaReport', className: 'btn-sm'},
            {extend: 'print',className: 'btn-sm'}
            ]
        });

	});

	var globalOptions = {
        responsive: true,
        legend: {
            labels:{
                fontColor:"#90969D"
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                    fontColor: "#90969D"
                },
                gridLines: {
                    color: "#37393F"
                }
            }],
            yAxes: [{
                ticks: {
                    fontColor: "#90969D"
                },
                gridLines: {
                    color: "#37393F"
                }
            }]
        }
    };

    var singleBarData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
        {
            label: "Data 0",
            backgroundColor: "rgba(227,6,19, 0.7)",
            borderColor: "rgba(227,6,19, 0.7)",
            borderWidth: 1,
            data: [15, 20, 30, 40, 30, 50, 60]
        }
        ]
    };
    var c4 = document.getElementById("singleBarOptions").getContext("2d");
    new Chart(c4, {type: 'bar', data: singleBarData, options: globalOptions});
</script>

</body>

</html>