<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>Auctor | CTI Monitor</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>

<!-- Wrapper-->
<div class="wrapper">

    <!-- Header-->
<!--     <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <div id="mobile-menu">
                    <div class="left-nav-toggle">
                        <a href="#">
                            <i class="stroke-hamburgermenu"></i>
                        </a>
                    </div>
                </div>
                <a class="navbar-brand" href="panelControl.html">
                    Auctor                    <span>v.1.3</span>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div class="left-nav-toggle">
                    <a href="">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class=" profil-link">
                        <a href="panelControl.html">
                            <span class="profile-address">Deysi.Pautt@constructoracolpatria.com</span>
                            <img src="images/profile.jpg" class="img-circle" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav> -->
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <!-- Navigation-->
    <aside class="navigation">
        <nav>
            <ul class="nav luna-nav">
                <li class="nav-category">
                    Principal
                </li>
                <li class="inactive">
                    <a href="panelControl.php">Panel de Control</a>
                </li>
                <li class="nav-category">
                    Llamadas
                </li>
                <li class="active">
                    <a href="consultas.php">Consultas</a>
                <li class="nav-category">
                    Estad&iacute;sticas
                </li>
                <li class="inactive">
                    <a href="estadisticas.php">Estad&iacute;sticas de Uso</a>
                <li class="inactive">
                    <a href="historico.php">Hist&oacute;rico de Reportes e Informes</a>
                <li class="inactive">
                    <a href="colores_1.php">Colores 1</a>
                <li class="inactive">
                    <a href="colores_2.php">Colores 2</a>
                <li class="inactive">
                    <a href="colores_3.php">Colores 3</a>
                <li class="nav-info">
                    <div class="m-t-xs">
                        <span class="c-white">Auctor</span> | CTI Monitor.
                    </div>
                </li>
            </ul>
        </nav>
    </aside>
    <!-- End navigation-->


    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">


            <div class="row">
                <div class="col-lg-12">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-graph3"></i>
                        </div>
                        <div class="header-title">
                            <h3>Estad&iacute;sticas de Uso</h3>
                            <small>
                                Gr&aacute;ficas relevantes
                            </small>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>


            <div class="row">

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                                <a class="panel-close"><i class="fa fa-times"></i></a>
                            </div>
                            Barras
                        </div>
                        <div class="panel-body">
                            <div>
                                <canvas id="barOptions" height="180"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                                <a class="panel-close"><i class="fa fa-times"></i></a>
                            </div>
                            Lineas
                        </div>
                        <div class="panel-body">
                            <div>
                                <canvas id="lineOptions" height="180"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                                <a class="panel-close"><i class="fa fa-times"></i></a>
                            </div>
                            Barras simples
                        </div>
                        <div class="panel-body">
                            <div>
                                <canvas id="singleBarOptions" height="140"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                                <a class="panel-close"><i class="fa fa-times"></i></a>
                            </div>
                            Lineas curvas
                        </div>
                        <div class="panel-body">
                            <div>
                                <canvas id="sharpLineOptions" height="140"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                                <a class="panel-close"><i class="fa fa-times"></i></a>
                            </div>
                            Polar
                        </div>
                        <div class="panel-body">
                            <div>
                                <canvas id="polarOptions" height="180"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                                <a class="panel-close"><i class="fa fa-times"></i></a>
                            </div>
                            Barras en dona
                        </div>
                        <div class="panel-body">
                            <div>
                                <canvas id="doughnutChart" height="180"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="vendor/pacejs/pace.min.js"></script>
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/chart.js/dist/Chart.min.js"></script>

<!-- App scripts -->
<script src="scripts/luna.js"></script>

<script>
    $(document).ready(function () {


        /**
         * Options for Line chart
         */

        var globalOptions = {
            responsive: true,
            legend: {
                labels:{
                    fontColor:"#90969D"
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        fontColor: "#90969D"
                    },
                    gridLines: {
                        color: "#37393F"
                    }
                }],
                yAxes: [{
                    ticks: {
                        fontColor: "#90969D"
                    },
                    gridLines: {
                        color: "#37393F"
                    }
                }]
            }
        };

        var lineData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [

                {
                    label: "Data 1",
                    backgroundColor: 'transparent',
                    borderColor: "#0AB04C",
                    pointBorderWidth: 0,
                    pointRadius: 2,
                    pointBorderColor: '#0AB04C',
                    borderWidth: 1,
                    data: [16, 32, 18, 26, 42, 33, 44]
                },
                {
                    label: "Data 2",
                    backgroundColor: 'transparent',
                    borderColor: "#1679B0",
                    pointBorderWidth: 0,
                    pointRadius: 2,
                    pointBorderColor: '#1679B0',
                    borderWidth: 1,
                    data: [22, 44, 67, 43, 76, 45, 12]
                }
            ]
        };

        var c1 = document.getElementById("lineOptions").getContext("2d");
        new Chart(c1, {type: 'line', data: lineData, options: globalOptions});

        /**
         * Options for Sharp Line chart
         */
        var sharpLineData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "Example dataset",
                    backgroundColor: 'rgba(10, 176, 76, 0.1)',
                    borderColor: "#0AB04C",
                    pointBorderWidth: 0,
                    pointRadius: 2,
                    pointBorderColor: '#0AB04C',
                    borderWidth: 1,
                    data: [33, 48, 40, 19, 54, 27, 54],
                    lineTension: 0
                }
            ]
        };

        var c2 = document.getElementById("sharpLineOptions").getContext("2d");
        new Chart(c2, {type: 'line', data: sharpLineData, options: globalOptions});


        /**
         * Data for Bar chart
         */
        var barData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "Data 1",
                    backgroundColor: 'transparent',
                    borderColor: "#0AB04C",
                    borderWidth: 1,
                    data: [9, 8, 5, 6, 3, 2, 16]

                },
                {
                    label: "Data 2",
                    backgroundColor: 'transparent',
                    borderColor: "#1679B0",
                    borderWidth: 1,
                    data: [5, 5, 5, 4, 5, 2, 23]
                }
            ]
        };

        var c3 = document.getElementById("barOptions").getContext("2d");
        new Chart(c3, {type: 'bar', data: barData, options: globalOptions});

        /**
         * Options for Bar chart
         */

        /**
         * Data for Bar chart
         */
        var singleBarData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "Data 0",
                    backgroundColor: '#FFFFFF',
                    borderColor: "#0AB04C",
                    borderWidth: 1,
                    data: [15, 20, 30, 40, 30, 50, 60]
                }
            ]
        };

        var c4 = document.getElementById("singleBarOptions").getContext("2d");
        new Chart(c4, {type: 'bar', data: singleBarData, options: globalOptions});


        var polarData = {
            datasets: [{
                data: [
                    120,
                    130,
                    100
                ],
                borderWidth: 0,
                backgroundColor: [
                    "rgba(227,6,19, 0.1)",
                    "rgba(10, 176, 76, 0.7)",
                    "rgba(22, 121, 176, 0.8)"
                ],
                label: 'My dataset' // for legend
            }],
            labels: [
                "Homer",
                "Inspinia",
                "Luna"
            ]
        };

        var c5 = document.getElementById("polarOptions").getContext("2d");
        new Chart(c5, {type: 'polarArea', data: polarData, options: globalOptions});


        var doughnutData = {
            labels: [
                "App",
                "Software",
                "Laptop"
            ],
            datasets: [
                {
                    data: [20, 120, 100],
                    borderWidth: 0,
                    backgroundColor: [
                        "rgba(227,6,19, 0.1)",
                        "rgba(10, 176, 76, 0.7)",
                        "rgba(22, 121, 176, 0.8)"
                    ],
                    hoverBackgroundColor: [
                        "#F6A821",
                        "#F6A821",
                        "#F6A821"
                    ]
                }]
        };


        var c6 = document.getElementById("doughnutChart").getContext("2d");
        new Chart(c6, {type: 'doughnut', data: doughnutData, options: globalOptions});

    });
</script>

</body>

</html>