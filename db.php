<?php
/* Database connection settings */
$host = 'localhost';
$user = 'root';
$pass = '';
$db = 'auctordb';
$mysqli = new mysqli($host, $user, $pass, $db) or die($mysqli->error);
$conn = new mysqli($host, $user, $pass, $db) or die($conn->error);

if ($mysqli->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>