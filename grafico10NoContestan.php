<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>Auctor | CTI Monitor</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="styles/style.css">


    <!-- Vendor scripts -->
    <script src="vendor/pacejs/pace.min.js"></script>
    <script src="vendor/jquery/dist/jquery.min.js"></script>

    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/chart.js/dist/Chart.min.js"></script>
    <script src="scripts/luna.js"></script>

    <style type="text/css">

</style>

</head>
<body>

    <!-- Wrapper-->
    <div class="wrapper">

    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    ?>
    <!-- End navigation-->

    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-graph3"></i>
                        </div>
                        <div class="header-title">
                            <h3>Top 10 Extensiones que No contestan</h3>
                            <small>
                                Datos Estad&iacute;sticos
                            </small>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <div class="header-title">
                        <form action="#" method="post">
                            <p>Desde:
                                <input type="date" id="from" name="from" autocomplete="off" />
                                Hasta:
                                <input type="date" id="to" name="to" autocomplete="off"/>
                            </p>
                           
                            <input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
                        </form>
                    </div>
                    <hr>
                </div>
            </div>
         
    		<div class="col-md-12">
    			<div class="panel">
    				<div class="panel-body">
    					<div>
    						<canvas id="PorHblaInPorHblaOutTMO" height="180"></canvas>
    					</div>
    				</div>
    			</div>
    		</div>
    		<!-- End main content-->
    	</div>
    </section>

</div>
</body>
<!-- End wrapper-->
<?php

if(isset($_POST['from'])){
    $sql_bar1="SELECT directorio.Extension, funcionario, Number_Calls FROM `llamadas_perdidas_totales`,`directorio` WHERE directorio.Extension=llamadas_perdidas_totales.Extension AND `Filter` BETWEEN '".$_POST['from']."' AND '".$_POST['to']."'" ;

    $sql_bar1.="ORDER BY `llamadas_perdidas_totales`.`Number_Calls` DESC LIMIT 10";

}

else{
	$sql_bar1="SELECT directorio.Extension, funcionario, Number_Calls FROM `llamadas_perdidas_totales`,`directorio` WHERE directorio.Extension=llamadas_perdidas_totales.Extension ORDER BY `llamadas_perdidas_totales`.`Number_Calls` DESC LIMIT 10";
}
	
//echo $sql_bar1;

$rs = $mysqli->query($sql_bar1);

?>

<script src="scripts/luna.js"></script>

<script>

	$(document).ready(function () {
		var datos = [];

		<?php
		$i=0;
		while ($fila1 = $rs->fetch_row()) 
		{
			//-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora				
			echo "\n datos[".$i."] = [];";
			echo "\n datos[".$i."]['extension'] = '".$fila1[0]."';";
            echo "\n datos[".$i."]['nomb'] = '".$fila1[1]."';";
            echo "\n datos[".$i."]['num_lla'] = '".$fila1[2]."';";

			$i++;
		}		

		echo "\n";
		?>
		var i=0;
		
		var doughnutData2 = {
			labels: [
			],
			datasets: [
			{
				data: [],
				borderWidth: 0,
				backgroundColor: [
				"rgba(227,6,19, 0.7)",
				"rgba(22, 121, 176, 0.7)",
				"rgba(22,5,19, 0.8)",
                "rgba(27,16,119, 0.8)",
                "rgba(27,86,169, 0.8)",
                "rgba(29,16,189, 0.8)",
                "rgba(37,66,199, 0.8)",
                "rgba(57,76,129, 0.8)",
                "rgba(67,68,19, 0.8)",
                "rgba(177,69,19, 0.8)",

				],
				hoverBackgroundColor: [
				"#F6A821",
				"#F6A821",
				"#F6A821"
				]
			}]
		};

		while(datos[i]){
		
			doughnutData2.labels.push((datos[i]['extension']+' '+datos[i]['nomb']))       
            doughnutData2.datasets[0]["data"].push(datos[i]['num_lla'])

			i++;
		}
	
        /**
         * Options for Line chart
         */

         var globalOptions = {
         	responsive: true,
         	legend: {
         		labels:{
         			fontColor:"#90969D"
         		}
         	},
         	scales: {
         		xAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}],
         		yAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}]
         	}
         };
        var c6 = document.getElementById("PorHblaInPorHblaOutTMO").getContext("2d");
        new Chart(c6, {type: 'polarArea', data: doughnutData2, options: globalOptions});
     });
 </script>

</body>

</html>