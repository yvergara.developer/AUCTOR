<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

  <!-- Page title -->
  <title>Auctor | CTI Monitor</title>

  <!-- Vendor styles -->
  <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
  <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

  <!-- App styles -->
  <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
  <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
  <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
  <link rel="stylesheet" href="styles/style.css">


  <!-- Vendor scripts -->
  <script src="vendor/pacejs/pace.min.js"></script>
  <script src="vendor/jquery/dist/jquery.min.js"></script>

  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="vendor/chart.js/dist/Chart.min.js"></script>
  <!----DatePicker------->
  
<!-- App scripts -->
<script src="scripts/luna.js"></script>

<style type="text/css">

</style>

</head>
<body>
	<!-- Wrapper-->
	<div class="wrapper">
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    ?>
    <!-- End navigation-->


    <!-- Main content-->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-12">
    				<div class="view-header">
    					<div class="header-icon">
    						<i class="pe page-header-icon pe-7s-graph3"></i>
    					</div>
    					<div class="header-title">
    						<h3>Contador de Llamadas por Opci&oacute;n de I.V.R.</h3>
    						<small>
    							Datos Estad&iacute;sticos
    						</small>
    					</div>
    				</div>
    				<hr>
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-12">

    				<div class="header-title">
    					<form action="#" method="post">
                        <p>
                            Desde
                            <input type="date" id="desde" name="desde" autocomplete="off" />
                            Hasta:
                            <input type="date" id="hasta" name="hasta" autocomplete="off"/>
                        </p>

                        <p>
                            GSM1:
                            <input type="checkbox" id="GSM1" name="GSM1" value="GSM1" />
                            GSM2:
                            <input type="checkbox" id="GSM2" name="GSM2" value="GSM2"/>
                        </p>
                        <input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
                    </form>
                </div>
                <hr>
            </div>
        </div>

        <div class="row">

         <div class="col-md-12">
            <div class="panel">
             <div class="panel-body">
              <div>
                 <canvas id="llamadasAbandonadasEntrantesAtendidas" height="180"></canvas>
             </div>
         </div>
     </div>
 </div>
</div>

<!-- End main content-->
</div>
</section>

</div>
</body>
<!-- End wrapper-->
<?php
if(isset($_POST['desde'])){
	  $sql_bar1="SELECT DATE_FORMAT(Time, '%Y-%m-%d'),`Source Trunk`,COUNT(`Disposition`) FROM `callog` WHERE DATE_FORMAT(Time, '%Y-%m-%d') BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."'" ;
if(isset($_POST['GSM1'])&&isset($_POST['GSM2'])){
        $sql_bar1.=" AND `Source Trunk` IN('GSM1','GSM2') ";
    }
    else if(isset($_POST['GSM1'])){
        $sql_bar1.=" AND `Source Trunk`='GSM1' ";
    }
    else if(isset($_POST['GSM2'])){
        $sql_bar1.=" AND `Source Trunk`='GSM2' ";
    }
    else{

    }
      $sql_bar1.="GROUP BY DATE_FORMAT(Time, '%Y-%m-%d'),`Source Trunk`";
    }

else{
  $sql_bar1="SELECT DATE_FORMAT(Time, '%Y-%m-%d'),`Source Trunk`,COUNT(`Disposition`) AS Dur FROM `callog` WHERE 1 GROUP BY DATE_FORMAT(Time, '%Y-%m-%d'),`Source Trunk`";

}

//echo $sql_bar1;

$rs = $mysqli->query($sql_bar1);

?>

<script>

	$(document).ready(function () {
        open();
        
		var datos = [];
		var datos2 = [];
         var dat1=[];
        var dat2=[];
         var barData = {
         labels: [],
         datasets: [
         {
            label: "GSM1",
            backgroundColor: "rgba(227,6,19, 0.7)",
            borderColor: "rgba(227,6,19, 0.7)",
            borderWidth: 1,
            data: []
        },
        {
            label: "GSM2",
            backgroundColor: '#1679B0',
            borderColor: "#1679B0",
            borderWidth: 1,
            data: []
        }, 
        
        ]
    };

		<?php
		$i=0;
		while ($fila1 = $rs->fetch_row()) 
		{
			//-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora				
			
			echo "\n datos[".$i."] = [];";
			echo "\n datos[".$i."]['fecha'] = '".$fila1[0]."';";
            echo "\n datos[".$i."]['1'] = '".$fila1[1]."';";
            echo "\n datos[".$i."]['2'] = '".$fila1[2]."';";
            if($fila1[1]=="GSM1"){
            echo "\n dat1.push('".$fila1[2]."');";

            }
            else{
            echo "\n dat2.push('".$fila1[2]."');";

            }

            $i++;
        }
        echo "\n";
        ?>
        
    var i=0;
        while(datos[i]){
     barData.labels.push(datos[i]['fecha'])
     if(datos[i]['1']=="GSM1"){
     dat1.push(datos[i]['2'])
    }
    else{
     dat2.push(datos[i]['2'])
    }

     i++;
    }
    barData.datasets[0]["data"]=dat1;
    barData.datasets[1]["data"]=dat2;

        /**
         * Options for Line chart
         */

         var globalOptions = {
         	responsive: true,
         	legend: {
         		labels:{
         			fontColor:"#90969D"
         		}
         	},
         	scales: {
         		xAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}],
         		yAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}]
         	}
         };

         var c1 = document.getElementById("llamadasAbandonadasEntrantesAtendidas").getContext("2d");
         new Chart(c1, {type: 'bar', data: barData, options: globalOptions});
     });
 </script>

</body>

</html>