<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

  <!-- Page title -->
  <title>Auctor | CTI Monitor</title>

  <!-- Vendor styles -->
  <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
  <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

  <!-- App styles -->
  <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
  <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
  <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
  <link rel="stylesheet" href="styles/style.css">


  <!-- Vendor scripts -->
  <script src="vendor/pacejs/pace.min.js"></script>
  <script src="vendor/jquery/dist/jquery.min.js"></script>

  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="vendor/chart.js/dist/Chart.min.js"></script>
  <!----DatePicker------->

<!-- App scripts -->
<script src="scripts/luna.js"></script>

<style type="text/css">

</style>


</head>
<body>

	<!-- Wrapper-->
	<div class="wrapper">
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    ?>
    <!-- End navigation-->


    <!-- Main content-->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-12">
    				<div class="view-header">
    					<div class="header-icon">
    						<i class="pe page-header-icon pe-7s-graph3"></i>
    					</div>
    					<div class="header-title">
    						<h3>Llamadas Atendidas por Agente (Cortas - Largas)</h3>
    						<small>
    							Datos Estad&iacute;sticos
    						</small>
    					</div>
    				</div>
    				<hr>
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-12">

    				<div class="header-title">
    					<form action="#" method="post">
    						<p>Desde
                                <input type="date" id="desde" name="desde" autocomplete="off" />
                                Hasta:
                                <input type="date" id="hasta" name="hasta" autocomplete="off"/>
                            </p>
                            <p>Usuario</p>
                            <select name="usr" class="select2_demo_2 form-control" style="width: 10%">
                             <option value=''> </option>
                             <?php
                             $consulta = "SELECT DISTINCT `Codigo_Piloto` FROM `diario_piloto` WHERE 1 ORDER BY `Codigo_Piloto`  ASC";
                             $resultado = $mysqli->query($consulta);
                             while ($fila = $resultado->fetch_row()) 
                             {
                                echo "<option value=$fila[0]>$fila[0]</option>";
                            }
                            ?>
                        </select>

                        <div class="header-title">    				
    						<p>Ventas:
    							<input type="checkbox" id="Ventas" name="Ventas" value="701" />
    						</p>
    						<p>
    							SAC:
    							<input type="checkbox" id="SAC" name="SAC" value="700"/>
    						</p>
    						<p>
    							Conmutador:
    							<input type="checkbox" id="Conmutador" name="Conmutador" value="704"/>
    						</p>
    						<input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
                        </div>
                    </form>
                </div>
                <hr>
            </div>
        </div>
        <div class="row">
         <div class="col-md-12">
            <div class="panel">
             <div class="panel-body">
              <div>
                 <canvas id="llamadasAtendidasCortasLargas" height="180"></canvas>
             </div>
         </div>
     </div>
 </div>


</div>

<!-- End main content-->
</div>
</section>


</div>
</body>
<!-- End wrapper-->
<?php
if(isset($_POST['desde'])){
	if($_POST['usr']==""){
		$sql_bar1="SELECT `Codigo_Piloto`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`St_Piloto_Llamadas_Atendidas_Antes_5_Segundos`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE `Fecha` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."'";
        if(isset($_POST['Ventas'])&&isset($_POST['SAC'])&&isset($_POST['Conmutador'])){
            $sql_bar1.=" AND Codigo_Piloto IN(701,700,704)";
        }
        else if(isset($_POST['Ventas'])&&isset($_POST['Conmutador'])){
            $sql_bar1.=" AND Codigo_Piloto IN(701,704)";
        }
        else if(isset($_POST['Ventas'])&&isset($_POST['SAC'])){
            $sql_bar1.=" AND Codigo_Piloto IN(701,700)";
        }
        else if(isset($_POST['SAC'])&&isset($_POST['Conmutador'])){
            $sql_bar1.=" AND Codigo_Piloto IN(700,704)";
        }
        else if(isset($_POST['Ventas'])){
            $sql_bar1.=" AND Codigo_Piloto=701";
        }
        else if(isset($_POST['SAC'])){
            $sql_bar1.=" AND Codigo_Piloto=700";
        }
        else if(isset($_POST['Conmutador'])){
            $sql_bar1.=" AND Codigo_Piloto=704";
        }
        $sql_bar1.=" GROUP BY `Codigo_Piloto`";
    }
    else{
      $sql_bar1="SELECT `Codigo_Piloto`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`St_Piloto_Llamadas_Atendidas_Antes_5_Segundos`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE `Codigo_Piloto`='".$_POST['usr']."' AND `Fecha` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."' GROUP BY `Codigo_Piloto`";


  }

}
else{
	$sql_bar1="SELECT `Codigo_Piloto`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`St_Piloto_Llamadas_Atendidas_Antes_5_Segundos`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto GROUP BY `Codigo_Piloto`";
	
}

$rs = $mysqli->query($sql_bar1);


?>


<script>

	$(document).ready(function () {
        open();
        
        var datos = [];
        var datos2 = [];



        <?php
        $i=0;
        while ($fila1 = $rs->fetch_row()) 
        {
			//-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora				
           $sum_entrant=intval($fila1[1])+intval($fila1[2])+intval($fila1[3]);
           $aten=$sum_entrant-intval($fila1[4]);
           echo "\n datos[".$i."] = [];";
           echo "\n datos[".$i."]['fecha'] = '".$fila1[0]."';";
           echo "\n datos[".$i."]['Ll_ent'] = '".$sum_entrant."';";
           echo "\n datos[".$i."]['Ll_aba'] = '".$fila1[4]."';";
           echo "\n datos[".$i."]['Ll_aten'] = '".$aten."';";
			//--------Llamadas atendidas por agente Cortas - Largas
           $sum_cort=intval($fila1[5])+intval($fila1[6]);
           $ll_largas=$sum_entrant-$sum_cort;
           echo "\n datos[".$i."]['ll_cortas'] = '".$sum_cort."';";
           echo "\n datos[".$i."]['ll_largas'] = '".$ll_largas."';";
			//-------Tiempo Medio de Operación
           echo "\n datos[".$i."]['tm_ttl'] = '".$fila1[5]."';";
           echo "\n datos[".$i."]['tm_prom'] = '".$fila1[6]."';";
           echo "\n datos[".$i."]['tm_ll_con'] = '".$fila1[7]."';";
           $i++;
       }
       $i=0;

       echo "\n";
       ?>
       var i=0;

       var barData2 = {
           labels: [],
           datasets: [
           {
            label: "Llamadas cortas < 15 (GENERAL)",
            backgroundColor: "rgba(227,6,19, 0.7)",
            borderColor: "rgba(227,6,19, 0.7)",
            borderWidth: 1,
            data: []
        },
        {
            label: "Llamadas Largas",
            backgroundColor: '#1679B0',
            borderColor: "#1679B0",
            borderWidth: 1,
            data: []
        }, 

        ]
    };





    while(datos[i]){
       barData2.labels.push(datos[i]['fecha'])

       barData2.datasets[0]["data"].push(datos[i]['ll_cortas'])
       barData2.datasets[1]["data"].push(datos[i]['ll_largas'])


       i++;
   }


        /**
         * Options for Line chart
         */

         var globalOptions = {
         	responsive: true,
         	legend: {
         		labels:{
         			fontColor:"#90969D"
         		}
         	},
         	scales: {
         		xAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}],
         		yAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}]
         	}
         };




         var c2 = document.getElementById("llamadasAtendidasCortasLargas").getContext("2d");
         new Chart(c2, {type: 'bar', data: barData2, options: globalOptions});



     });
 </script>

</body>

</html>