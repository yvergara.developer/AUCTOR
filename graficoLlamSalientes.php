<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

  <!-- Page title -->
  <title>Auctor | CTI Monitor</title>

  <!-- Vendor styles -->
  <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
  <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

  <!-- App styles -->
  <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
  <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
  <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
  <link rel="stylesheet" href="styles/style.css">

  <!-- Vendor scripts -->
  <script src="vendor/pacejs/pace.min.js"></script>
  <script src="vendor/jquery/dist/jquery.min.js"></script>

  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="vendor/chart.js/dist/Chart.min.js"></script>
  <!----DatePicker------->
  
<!-- App scripts -->
<script src="scripts/luna.js"></script>

<style type="text/css">

</style>

</head>
<body>

	<!-- Wrapper-->
	<div class="wrapper">
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    ?>
    <!-- End navigation-->

    <!-- Main content-->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-12">
    				<div class="view-header">
    					<div class="header-icon">
    						<i class="pe page-header-icon pe-7s-graph3"></i>
    					</div>
    					<div class="header-title">
    						<h3>Llamadas Salientes, Locales y Externas</h3>
    						<small>
    							Datos Estad&iacute;sticos
    						</small>
    					</div>
    				</div>
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-12">
    				<div class="header-title">
    					<form action="#" method="post">
    						<p>Desde
                                <input type="date" id="desde" name="desde" autocomplete="off" />
                                Hasta:
                                <input type="date" id="hasta" name="hasta" autocomplete="off"/>
                            </p>
                            <p>Piloto:</p>
                            <p>Ventas:
                                <input type="checkbox" id="Ventas" name="Ventas" value="701" />
                            </p>
                            <p>
                                SAC:
                                <input type="checkbox" id="SAC" name="SAC" value="700"/>
                            </p>
                            <p>
                                Conmutador:
                                <input type="checkbox" id="Conmutador" name="Conmutador" value="704"/>
                            </p>
                            
                            <p>Usuario</p>
                            <select name="usr" class="select2_demo_2 form-control" style="width: 10%">
                             <option value=''> </option>
                             <?php
                             $consulta = "SELECT DISTINCT CONCAT(`Agente_Numero`,' ' ,`Nombre_Agente`), `Agente_Numero` FROM `diario_agente`,`diario_sesion` WHERE Agente_Numero=Numero_Directorio ORDER BY `Agente_Numero` ASC";
                             $resultado = $mysqli->query($consulta);
                             while ($fila = $resultado->fetch_row()) 
                             {
                              echo "<option value=$fila[0]>$fila[0]</option>";
                            }
                          ?> 
                      </select>
                      <br><br>
                      <input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
                  </form>
              </div>
              <hr>
          </div>
      </div>    		

      <div class="row">

         <div class="col-md-12">
            <div class="panel">
             <div class="panel-body">
              <div>
                 <canvas id="LlamadasSalientesLocalesExternas" height="180"></canvas>
             </div>
         </div>
     </div>
 </div>

</div>

<!-- End main content-->
</div>
</section>

</div>
</body>
<!-- End wrapper-->
<?php
if(isset($_POST['desde'])){
	if($_POST['usr']==""){
		$sql_bar2="SELECT `agente_numero`, SUM(`Salidas_Locales`) AS Sal_Loc, SUM(`Llegadas_ACD_Salientes_Respondidas`) AS Ll_Acd_Res FROM diario_agente WHERE `Fecha` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."' GROUP BY `agente_numero`";
	}
	else{
		$sql_bar2="SELECT `agente_numero`, SUM(`Salidas_Locales`) AS Sal_Loc, SUM(`Llegadas_ACD_Salientes_Respondidas`) AS Ll_Acd_Res FROM diario_agente WHERE `agente_numero`='".$_POST['usr']."' AND  `Fecha` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."' GROUP BY `agente_numero`";
	}
}
else{
	$sql_bar2="SELECT `agente_numero`, SUM(`Salidas_Locales`) AS Sal_Loc, SUM(`Llegadas_ACD_Salientes_Respondidas`) AS Ll_Acd_Res FROM diario_agente WHERE `Fecha` GROUP BY `agente_numero`";
}

$rs2 = $mysqli->query($sql_bar2);
?>
<script>

	$(document).ready(function () {
        open();
        
        var datos = [];
        var datos2 = [];

        <?php
        
        
        $i=0;
        while ($fila2 = $rs2->fetch_row()) 
        {
			//-------Llamadas Salientes Locales y Externas				
         ?>
         if(datos['<?php echo $i; ?>']==undefined){
            datos['<?php echo $i; ?>'] = [];
        }
        <?php 
        echo "\n datos[".$i."]['fecha2'] = '".$fila2[0]."';";
        echo "\n datos[".$i."]['Ll_sal'] = '".$fila2[1]."';";
        echo "\n datos[".$i."]['Ll_sal_ext'] = '".$fila2[2]."';";

        $i++;
    }
    
    echo "\n";
    ?>
    var i=0;

    var barData3 = {
     labels: [],
     datasets: [
     {
        label: "LLAMADAS SALIENTES LOCALES",
        backgroundColor: "rgba(227,6,19, 0.7)",
        borderColor: "rgba(227,6,19, 0.7)",
        borderWidth: 1,
        data: []
    },
    {
        label: "LLAMADAS SALIENTES EXTERNAS",
        backgroundColor: '#1679B0',
        borderColor: "#1679B0",
        borderWidth: 1,
        data: []
    }, 
    
    ]
};

while(datos[i])
{
 barData3.labels.push(datos[i]['fecha2'])
 barData3.datasets[0]["data"].push(datos[i]['Ll_sal'])
 barData3.datasets[1]["data"].push(datos[i]['Ll_sal_ext'])
i++;
}	
        /**
         * Options for Line chart
         */
         var globalOptions = {
         	responsive: true,
         	legend: {
         		labels:{
         			fontColor:"#90969D"
         		}
         	},
         	scales: {
         		xAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}],
         		yAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}]
         	}
         };
         var c3 = document.getElementById("LlamadasSalientesLocalesExternas").getContext("2d");
         new Chart(c3, {type: 'bar', data: barData3, options: globalOptions});
     });
 </script>
</body>
</html>