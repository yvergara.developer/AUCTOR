<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

  <!-- Page title -->
  <title>Auctor | CTI Monitor</title>

  <!-- Vendor styles -->
  <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
  <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

  <!-- App styles -->
  <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
  <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
  <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
  <link rel="stylesheet" href="styles/style.css">


  <!-- Vendor scripts -->
  <script src="vendor/pacejs/pace.min.js"></script>
  <script src="vendor/jquery/dist/jquery.min.js"></script>

  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="vendor/chart.js/dist/Chart.min.js"></script>
  <!----DatePicker------->
  <!-- App scripts -->
  <script src="scripts/luna.js"></script>

  <style type="text/css">

</style>

</head>
<body>

  <!-- Wrapper-->
  <div class="wrapper">
      <?php
      include("cabecera.php");
      ?>
      <!-- End header-->

      <!-- Navigation-->
      <?php
      include("menu.php");
      ?>
      <!-- End navigation-->


      <!-- Main content-->
      <section class="content">
        <div class="container-fluid">


          <div class="row">
            <div class="col-lg-12">
              <div class="view-header">
                <div class="header-icon">
                  <i class="pe page-header-icon pe-7s-graph3"></i>
                </div>
                <div class="header-title">
                  <h3>Porcentaje (%) Tiempo Conectado</h3>
                  <small>
                    Datos Estad&iacute;sticos
                  </small>
                </div>
              </div>
              <hr>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">

              <div class="header-title">
                <form action="#" method="post">
                 <p>Desde
                  <input type="date" id="desde" name="desde" autocomplete="off" />
                  Hasta:
                  <input type="date" id="hasta" name="hasta" autocomplete="off"/>
                </p>
                <select name="usr" class="select2_demo_2 form-control" style="width: 10%">
                 <?php
                 $consulta = "SELECT DISTINCT CONCAT(`Agente_Numero`,' ' ,`Nombre_Agente`), `Agente_Numero` FROM `diario_agente`,`diario_sesion` WHERE Agente_Numero=Numero_Directorio ORDER BY `Agente_Numero` ASC";
                 $resultado = $mysqli->query($consulta);
                 while ($fila = $resultado->fetch_row()) {

                  echo "<option value=$fila[0]>$fila[0]</option>";

                }
                ?> 
              </select>
              <br>
              <input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
            </form>
          </div>
          <hr>
        </div>
      </div>


      <div class="row">

       <div class="col-md-12">
        <div class="panel">
       <div class="panel-body">
        <div>
         <canvas id="llamadasAbandonadasEntrantesAtendidas" height="180"></canvas>
       </div>
     </div>
   </div>
 </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-filled">
      <div class="panel-heading">

      </div>
      <div class="panel-body">
        <p>
          Hist&oacute;rico
        </p>
        <div class="table-responsive">

          <table id="tableExample3" class="table table-striped table-hover">
            <thead>
              <tr align='center'>
                <th>Break</th>
                <th>Baño</th>
                <th>Almuerzo</th>
                <th>Capacitacion</th>
                <th>Tarea</th>
                <th>Diario Logon</th>

              </tr>
            </thead>
            <tbody>
              <?php
              if(isset($_POST['desde'])){
                $sql_bar1="SELECT SUM(diario_agente.St_Agente_Retirado_Causa_1), SUM(diario_agente.St_Agente_Retirado_Causa_2), SUM(diario_agente.St_Agente_Retirado_Causa_3),SUM(diario_agente.St_Agente_Retirado_Causa_4),SUM(diario_agente.St_Agente_Retirado_Causa_5),SUM(diario_agente.St_Agente_Retirado_Causa_6), (TIME_TO_SEC(diario_sesion.Duracion_Logon))/60 FROM diario_agente, diario_sesion WHERE diario_agente.Fecha=diario_sesion.Dia AND diario_agente.Agente_Numero=diario_sesion.Numero_Directorio AND diario_agente.Fecha BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."' AND  diario_agente.Agente_Numero='".$_POST['usr']."'";


              }
              else
              {
                $sql_bar1="SELECT SUM(diario_agente.St_Agente_Retirado_Causa_1), SUM(diario_agente.St_Agente_Retirado_Causa_2), SUM(diario_agente.St_Agente_Retirado_Causa_3),SUM(diario_agente.St_Agente_Retirado_Causa_4),SUM(diario_agente.St_Agente_Retirado_Causa_5),SUM(diario_agente.St_Agente_Retirado_Causa_6), (TIME_TO_SEC(diario_sesion.Duracion_Logon))/60 FROM diario_agente, diario_sesion WHERE diario_agente.Fecha=diario_sesion.Dia AND diario_agente.Agente_Numero=diario_sesion.Numero_Directorio AND diario_agente.Fecha AND diario_agente.Agente_Numero";
              }

              $resultado = $mysqli->query($sql_bar1);
              while ($fila = $resultado->fetch_row()) {

               echo "<tr align='center'>";
               echo "<td>$fila[1]";
               echo "<td>$fila[2]";
               echo "<td>$fila[3]";
               echo "<td>$fila[4]";
               echo "<td>$fila[5]";
               echo "<td>$fila[6]";

               echo "</tr>";
             }
             ?>

           </tbody>
         </table>
       </div>
     </div>
   </div>
 </div>
</div>

<!-- End main content-->
</div>
</section>

</div>
</body>
<!-- End wrapper-->
<?php
//echo $sql_bar1;
if(isset($_POST['desde'])){
  $sql_bar1="SELECT SUM(diario_agente.St_Agente_Retirado_Causa_1), SUM(diario_agente.St_Agente_Retirado_Causa_2), SUM(diario_agente.St_Agente_Retirado_Causa_3),SUM(diario_agente.St_Agente_Retirado_Causa_4),SUM(diario_agente.St_Agente_Retirado_Causa_5),SUM(diario_agente.St_Agente_Retirado_Causa_6), (TIME_TO_SEC(diario_sesion.Duracion_Logon))/60 FROM diario_agente, diario_sesion WHERE diario_agente.Fecha=diario_sesion.Dia AND diario_agente.Agente_Numero=diario_sesion.Numero_Directorio AND diario_agente.Fecha BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."' AND  diario_agente.Agente_Numero='".$_POST['usr']."'";

}
else{
  $sql_bar1="SELECT SUM(diario_agente.St_Agente_Retirado_Causa_1), SUM(diario_agente.St_Agente_Retirado_Causa_2), SUM(diario_agente.St_Agente_Retirado_Causa_3),SUM(diario_agente.St_Agente_Retirado_Causa_4),SUM(diario_agente.St_Agente_Retirado_Causa_5),SUM(diario_agente.St_Agente_Retirado_Causa_6), (TIME_TO_SEC(diario_sesion.Duracion_Logon))/60 FROM diario_agente, diario_sesion WHERE diario_agente.Fecha=diario_sesion.Dia AND diario_agente.Agente_Numero=diario_sesion.Numero_Directorio AND diario_agente.Fecha AND diario_agente.Agente_Numero";

}
$rs = $mysqli->query($sql_bar1);

?>
<script src="vendor/datatables/datatables.min.js"></script>

<script>

  $(document).ready(function () {
    open();

    $('#tableExample3').DataTable({
      dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      buttons: [

      {extend: 'csv',title: 'ExampleFile', className: 'btn-sm'},
      {extend: 'excelHtml5',title: 'ExampleFile', className: 'btn-sm'},            
      {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
      {extend: 'print',className: 'btn-sm'}
      ]
    });

    var datos = [];
    var datos2 = [];
    var algo ='';

    <?php
    $i=0;

    while ($fila1 = $rs->fetch_row()) 
    {
      //-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora              
      echo "\n datos[".$i."] = [];";
      echo "\n datos[".$i."]['1'] = '".$fila1[0]."';";
      echo "\n datos[".$i."]['2'] = '".$fila1[1]."';";
      echo "\n datos[".$i."]['3'] = '".$fila1[2]."';";
      echo "\n datos[".$i."]['4'] = '".$fila1[3]."';";
      echo "\n datos[".$i."]['5'] = '".$fila1[4]."';";
      echo "\n datos[".$i."]['6'] = '".$fila1[5]."';";
      echo "\n datos[".$i."]['7'] = '".$fila1[6]."';";

      $i++;
    }
    echo "\n";
    ?>
    var i=0;
    var doughnutData = {
      labels: [
      "Break",
      "Baño",
      "Almuerzo",
      "Recepcion",
      "Capacitacion",
      "Tarea",
      "Diario Logon",
      ],
      datasets: [
      {
        data: [],
        borderWidth: 0,
        backgroundColor: [
        "rgba(227,6,19, 0.1)",
        "rgba(255, 220, 20, 1)",
        "rgba(227,6,19, 1)",
        "rgba(22, 121, 176, 1)",
        "rgba(0,178,45, 1)",
        "#0009BD",
        "#750263"

        ],
        hoverBackgroundColor: [
        "#F6A821",
        "#F6A821",
        "#F6A821",
        "#F6A821",
        "#F6A821"

        ]
      }]
    };

    while(datos[i]){
     doughnutData.datasets[0]["data"].push(datos[i]['1'])
     doughnutData.datasets[0]["data"].push(datos[i]['2'])
     doughnutData.datasets[0]["data"].push(datos[i]['3'])
     doughnutData.datasets[0]["data"].push(datos[i]['4'])
     doughnutData.datasets[0]["data"].push(datos[i]['5'])
     doughnutData.datasets[0]["data"].push(datos[i]['6'])
     doughnutData.datasets[0]["data"].push(datos[i]['7'])
     i++;
   }

        /**
         * Options for Line chart
         */

         var globalOptions = {
          responsive: true,
          legend: {
            labels:{
              fontColor:"#90969D"
            }
          },
          scales: {
            xAxes: [{
              ticks: {
                fontColor: "#90969D"
              },
              gridLines: {
                color: "#37393F"
              }
            }],
            yAxes: [{
              ticks: {
                fontColor: "#90969D"
              },
              gridLines: {
                color: "#37393F"
              }
            }]
          }
        };

        var c1 = document.getElementById("llamadasAbandonadasEntrantesAtendidas").getContext("2d");
        new Chart(c1, {type: 'doughnut', data: doughnutData, options: globalOptions});

      });
    </script>

  </body>

  </html>