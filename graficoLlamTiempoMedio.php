<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>Auctor | CTI Monitor</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="styles/style.css">

    <!-- Vendor scripts -->
    <script src="vendor/pacejs/pace.min.js"></script>
    <script src="vendor/jquery/dist/jquery.min.js"></script>

    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/chart.js/dist/Chart.min.js"></script>
    
<!-- App scripts -->
<script src="scripts/luna.js"></script>
    <style type="text/css">
</style>
</head>
<body>

	<!-- Wrapper-->
	<div class="wrapper">
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    ?>
    <!-- End navigation-->

    <!-- Main content-->
    <section class="content">
    	<div class="container-fluid">

    		<div class="row">
    			<div class="col-lg-12">
    				<div class="view-header">
    					<div class="header-icon">
    						<i class="pe page-header-icon pe-7s-graph3"></i>
    					</div>
    					<div class="header-title">
    						<h3>Tiempo Medio y Total de Operaci&oacute;n</h3>
    						<small>
    							Datos Estad&iacute;sticos
    						</small>
    					</div>
    				</div>
    				<hr>
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-12">

    				<div class="header-title">
    					<form action="#" method="post">
						  <p>Desde
                                <input type="date" id="desde" name="desde" autocomplete="off" />
                                Hasta:
                                <input type="date" id="hasta" name="hasta" autocomplete="off"/>
                            </p>
                            <p>Ventas:
                                <input type="checkbox" id="Ventas" name="Ventas" value="701" />
                            </p>
                            <p>
                                SAC:
                                <input type="checkbox" id="SAC" name="SAC" value="700"/>
                            </p>
                            <p>
                                Conmutador:
                                <input type="checkbox" id="Conmutador" name="Conmutador" value="704"/>
                            </p>

    						<br>
							<input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
    					</form>
    				</div>
    				<hr>
    			</div>
    		</div>

    		<div class="col-md-12">
    			<div class="panel">
    				<div class="panel-heading">
                    Tiempo Medio de Operaci&oacute;n | Agentes
    				</div>
    				<div class="panel-body">
    					<div>
    						<canvas id="tiempoMedioOperacion" height="180"></canvas>
    					</div>
    				</div>
    			</div>
    		</div>

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        Tiempo Medio de Operaci&oacute;n | Pilotos
                    </div>
                    <div class="panel-body">
                        <div>
                            <canvas id="tiempoMedioOperacion2" height="180"></canvas>
                        </div>
                    </div>
                </div>
            </div>
    		
    		<!-- End main content-->
    	</div>
    </section>

</div>
</body>
<!-- End wrapper-->
<?php
if(isset($_POST['desde']))
{
	$sql_bar1="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`St_Piloto_Llamadas_Atendidas_Antes_5_Segundos`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE `Fecha` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."'";
    if(isset($_POST['Ventas'])&&isset($_POST['SAC'])&&isset($_POST['Conmutador'])){
        $sql_bar1.=" AND Codigo_Piloto IN(701,700,704)";
    }
    else if(isset($_POST['Ventas'])&&isset($_POST['Conmutador'])){
        $sql_bar1.=" AND Codigo_Piloto IN(701,704)";
    }
    else if(isset($_POST['Ventas'])&&isset($_POST['SAC'])){
        $sql_bar1.=" AND Codigo_Piloto IN(701,700)";
    }
    else if(isset($_POST['SAC'])&&isset($_POST['Conmutador'])){
        $sql_bar1.=" AND Codigo_Piloto IN(700,704)";
    }
    else if(isset($_POST['Ventas'])){
        $sql_bar1.=" AND Codigo_Piloto=701";
    }
    else if(isset($_POST['SAC'])){
        $sql_bar1.=" AND Codigo_Piloto=700";
    }
    else if(isset($_POST['Conmutador'])){
        $sql_bar1.=" AND Codigo_Piloto=704";
    }
    $sql_bar1.=" GROUP BY `Fecha`";

    $sql_bar2="SELECT `Fecha`, SUM(`Tiempo_Conversacion_ACD`) FROM `diario_agente`,`diario_sesion` WHERE `diario_agente`.`Agente_Numero`=`diario_sesion`.`Numero_Directorio`
    AND `Fecha` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."'"; 
      if(isset($_POST['Ventas'])&&isset($_POST['SAC'])&&isset($_POST['Conmutador'])){
        $sql_bar2.=" AND `diario_sesion`.`Directorio_Piloto` IN(701,700,704)";
    }
    else if(isset($_POST['Ventas'])&&isset($_POST['Conmutador'])){
        $sql_bar2.=" AND `diario_sesion`.`Directorio_Piloto` IN(701,704)";
    }
    else if(isset($_POST['Ventas'])&&isset($_POST['SAC'])){
        $sql_bar2.=" AND `diario_sesion`.`Directorio_Piloto` IN(701,700)";
    }
    else if(isset($_POST['SAC'])&&isset($_POST['Conmutador'])){
        $sql_bar2.=" AND `diario_sesion`.`Directorio_Piloto` IN(700,704)";
    }
    else if(isset($_POST['Ventas'])){
        $sql_bar2.=" AND `diario_sesion`.`Directorio_Piloto`=701";
    }
    else if(isset($_POST['SAC'])){
        $sql_bar2.=" AND `diario_sesion`.`Directorio_Piloto`=700";
    }
    else if(isset($_POST['Conmutador'])){
        $sql_bar2.=" AND `diario_sesion`.`Directorio_Piloto`=704";
    }

    $sql_bar2.=" GROUP BY `Fecha`";

}
else{
	$sql_bar1="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`St_Piloto_Llamadas_Atendidas_Antes_5_Segundos`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto GROUP BY `Fecha`";

    $sql_bar2="SELECT `Fecha`, SUM(`Tiempo_Conversacion_ACD`) FROM `diario_agente`,`diario_sesion` WHERE `diario_agente`.`Agente_Numero`=`diario_sesion`.`Numero_Directorio` GROUP BY `Fecha`";
}
$rs = $mysqli->query($sql_bar1);
$rs2 = $mysqli->query($sql_bar2);
?>

<script>

	$(document).ready(function () {
        open();
		var datos = [];
		var datos2 = [];

		<?php
		$i=0;
		while ($fila1 = $rs->fetch_row()) 
		{
			echo "\n datos[".$i."] = [];";
			echo "\n datos[".$i."]['fecha'] = '".$fila1[0]."';";
			echo "\n datos[".$i."]['tm_ttl'] = '".$fila1[5]."';";
			echo "\n datos[".$i."]['tm_prom'] = '".$fila1[6]."';";
			echo "\n datos[".$i."]['tm_ll_con'] = '".$fila1[7]."';";
			$i++;
		}
        $i=0;
        while ($fila2 = $rs2->fetch_row()) 
        {
            echo "\n datos2[".$i."] = [];";             
            echo "\n datos2[".$i."]['fecha2'] = '".$fila2[0]."';";
            echo "\n datos2[".$i."]['tm_acd'] = '".$fila2[1]."';";
            $i++;
        }

		echo "\n";
		?>
		var i=0;
		
		var lineData = {
            labels: [],
            datasets: [

            {
                label: "Tiempo Total",
                backgroundColor: 'transparent',
                borderColor: "#E30613",
                pointBorderWidth: 0,
                pointRadius: 2,
                pointBorderColor: '#E30613',
                borderWidth: 1,
                data: []
            },
            {
                label: "Tiempo Promedio",
                backgroundColor: 'transparent',
                borderColor: "#1679B0",
                pointBorderWidth: 0,
                pointRadius: 2,
                pointBorderColor: '#1679B0',
                borderWidth: 1,
                data: []
            }
            ]
        };
        var lineData2 = {
            labels: [],
            datasets: [

            {
                label: "Tiempo Medio de Operación",
                backgroundColor: 'transparent',
                borderColor: "#E30613",
                pointBorderWidth: 0,
                pointRadius: 2,
                pointBorderColor: '#E30613',
                borderWidth: 1,
                data: []
            }
            ]
        };
	
		var ttl=0;
		var ttl2=0;
		var ttl3=0;

		while(datos[i])
        {
            lineData.labels.push(datos[i]['fecha'])
			lineData.datasets[0]["data"].push(datos[i]['tm_ttl'])
            lineData.datasets[1]["data"].push(datos[i]['tm_prom'])
            i++;
		}
        i=0;

        while(datos2[i])
        {    
            lineData2.labels.push(datos2[i]['fecha2'])
            lineData2.datasets[0]["data"].push(datos2[i]['tm_acd'])
            i++;
        }
	
        /**
         * Options for Line chart
         */

         var globalOptions = {
         	responsive: true,
         	legend: {
         		labels:{
         			fontColor:"#90969D"
         		}
         	},
         	scales: {
         		xAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}],
         		yAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}]
         	}
         };

        var c4 = document.getElementById("tiempoMedioOperacion").getContext("2d");
         new Chart(c4, {type: 'line', data: lineData, options: globalOptions});

        var c5 = document.getElementById("tiempoMedioOperacion2").getContext("2d");
         new Chart(c5, {type: 'line', data: lineData2, options: globalOptions});

     });
 </script>

</body>

</html>