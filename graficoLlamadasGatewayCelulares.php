<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

  <!-- Page title -->
  <title>Auctor | CTI Monitor</title>

  <!-- Vendor styles -->
  <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
  <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

  <!-- App styles -->
  <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
  <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
  <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
  <link rel="stylesheet" href="styles/style.css">

  <!-- Vendor scripts -->
  <script src="vendor/pacejs/pace.min.js"></script>
  <script src="vendor/jquery/dist/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="vendor/chart.js/dist/Chart.min.js"></script>  
  <script src="scripts/luna.js"></script>

<style type="text/css"></style>

</head>
<body>

    <!-- Wrapper-->
    <div class="wrapper">

    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    ?>
    <!-- End navigation-->

    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-graph3"></i>
                        </div>
                        <div class="header-title">
                            <h3>Llamadas Gateway Celular</h3>
                            <small>
                                Datos Estad&iacute;sticos
                            </small>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <div class="header-title">
                        <form action="#" method="post">
                         <p>Desde
                            <input type="date" id="desde" name="desde" autocomplete="off" />
                            Hasta:
                            <input type="date" id="hasta" name="hasta" autocomplete="off"/>
                        </p>
                        <br>
                        <input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
                    </form>
                </div>
                <hr>
            </div>
        </div>


        <div class="row">

         <div class="col-md-12">
            <div class="panel">
             <div class="panel-body">
              <div>
                 <canvas id="llamadasAbandonadasEntrantesAtendidas" height="180"></canvas>
             </div>
         </div>
     </div>
 </div>
</div>

<!-- End main content-->
</div>
</section>


</div>
</body>
<!-- End wrapper-->
<?php
if(isset($_POST['desde'])){
    $sql_bar1="SELECT DATE_FORMAT(Time, '%Y-%m-%d'),`Source Trunk`,COUNT(`Disposition`),`Disposition`,SUM(`Duration`) AS Dur FROM `callog` WHERE DATE_FORMAT(Time, '%Y-%m-%d') BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."' GROUP BY DATE_FORMAT(Time, '%Y-%m-%d'),`Source Trunk`,`Disposition`";


}
else{
    $sql_bar1="SELECT DATE_FORMAT(Time, '%Y-%m-%d'),`Source Trunk`,COUNT(`Disposition`),`Disposition`,SUM(`Duration`) AS Dur FROM `callog` WHERE 1 GROUP BY DATE_FORMAT(Time, '%Y-%m-%d'),`Source Trunk`,`Disposition`";


}

$rs = $mysqli->query($sql_bar1);


?>

<script>

    $(document).ready(function () {
        open();
        
        var datos = [];
        var datos2 = [];



        <?php
        $i=0;
        while ($fila1 = $rs->fetch_row()) 
        {
            //-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora              
            
            echo "\n datos[".$i."] = [];";
            echo "\n datos[".$i."]['fecha'] = '".$fila1[0]."';";
            echo "\n datos[".$i."]['tree'] = '".$fila1[1]."';";
            echo "\n datos[".$i."]['num_cal'] = '".$fila1[2]."';";
            echo "\n datos[".$i."]['count'] = '".$fila1[3]."';";
            echo "\n datos[".$i."]['dur'] = '".$fila1[4]."';";



            $i++;
        }
        echo "\n";
        ?>
        var i=0;
        var barData = {
         labels: [],
         datasets: [
         {
            label: "ANSWERED",
            backgroundColor: "rgba(227,6,19, 0.7)",
            borderColor: "rgba(227,6,19, 0.7)",
            borderWidth: 1,
            data: []
        },
        {
            label: "DURATION",
            backgroundColor: '#1679B0',
            borderColor: "#1679B0",
            borderWidth: 1,
            data: []
        }, 
        {
            label: "NO ANSWERED",
            backgroundColor: "rgba(255, 220, 20, 1)",
            borderColor: "rgba(255, 220, 20, 1)",
            borderWidth: 1,
            data: []
        },

        {
            label: "DURATION",
            backgroundColor: "rgba(0,178,45, 1)",
            borderColor: "rgba(0,178,45, 1)",
            borderWidth: 1,
            data: []
        },
        ]
    };


    while(datos[i]){
     barData.labels.push(datos[i]['fecha']+' '+datos[i]['tree'])
     if(datos[i]['count']=="ANSWERED"){
     barData.datasets[0]["data"].push(datos[i]['num_cal'])
     barData.datasets[1]["data"].push(datos[i]['dur'])

    }
    else{
     barData.datasets[2]["data"].push(datos[i]['num_cal'])
     barData.datasets[3]["data"].push(datos[i]['dur'])

    }

     i++;
    }
 





        /**
         * Options for Line chart
         */

         var globalOptions = {
            responsive: true,
            legend: {
                labels:{
                    fontColor:"#90969D"
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        fontColor: "#90969D"
                    },
                    gridLines: {
                        color: "#37393F"
                    }
                }],
                yAxes: [{
                    ticks: {
                        fontColor: "#90969D"
                    },
                    gridLines: {
                        color: "#37393F"
                    }
                }]
            }
         };




         var c1 = document.getElementById("llamadasAbandonadasEntrantesAtendidas").getContext("2d");
         new Chart(c1, {type: 'bar', data: barData, options: globalOptions});




     });
 </script>

</body>

</html>