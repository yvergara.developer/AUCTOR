<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>Auctor | CTI Monitor</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="styles/style.css">

    <!-- Vendor scripts -->
    <script src="vendor/pacejs/pace.min.js"></script>
    <script src="vendor/jquery/dist/jquery.min.js"></script>

    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/chart.js/dist/Chart.min.js"></script>
    <!----DatePicker------->

    <!-- App scripts -->
    <script src="scripts/luna.js"></script>

    <style type="text/css">

</style>

</head>
<body>

	<!-- Wrapper-->
	<div class="wrapper">

    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    ?>
    <!-- End navigation-->

    <!-- Main content-->
    <section class="content">
    	<div class="container-fluid">

    		<div class="row">
    			<div class="col-lg-12">
    				<div class="view-header">
    					<div class="header-icon">
    						<i class="pe page-header-icon pe-7s-graph3"></i>
    					</div>
    					<div class="header-title">
    						<h3>Llamadas Perdidas Por Vicepresidencia</h3>
    						<small>
    							Datos Estad&iacute;sticos
    						</small>
    					</div>
    				</div>
    				<hr>
    			</div>
    		</div>

           <div class="row">
            <div class="col-lg-12">

                <div class="header-title">
                    <form action="#" method="post">
                        <p>Desde:
                            <input type="date" id="from" name="from" autocomplete="off" />
                            Hasta:
                            <input type="date" id="to" name="to" autocomplete="off"/>
                        </p>

                        <input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
                    </form>
                </div>
                <hr>
            </div>
        </div>
        <div class="col-md-12">
           <div class="panel">
      <div class="panel-body">
         <div>
          <canvas id="PorHblaInPorHblaOutTMO" height="180"></canvas>
      </div>
  </div>
</div>
</div>


<div class="col-md-12">
   <div class="panel">
    <div class="panel-heading">
  Top 10 gerencias que No contestan   
</div>
<div class="panel-body">
 <div>
  <canvas id="PorHblaInPorHblaOutTMO2" height="180"></canvas>
</div>
</div>
</div>
</div>
<!-- End main content-->
</div>
</section>


</div>
</body>
<!-- End wrapper-->
<?php
if(isset($_POST['from'])){
    $sql_bar1="SELECT COUNT(*) AS ALG, `Cost_Center` FROM `llamadas_perdidas_detallado` WHERE `Cost_Center` IN('VIC TRANS','VIC GESTIO') AND `Date_Time` BETWEEN '".$_POST['from']."' AND '".$_POST['to']."'";
    $sql_bar1.="GROUP BY `Cost_Center`";

    $sql_bar2="SELECT COUNT(*) AS ALG, `Cost_Center` FROM `llamadas_perdidas_detallado` WHERE `Cost_Center` IN('G. LICITAC','GER COMERC','GER CONTRO','GER INFRAE', 
    'GER JURIDI','GER MERCAD','GER NEGOCI','GER SAC','GER TRANSA','GER. FINAN')  AND `Date_Time` BETWEEN '".$_POST['from']."' AND '".$_POST['to']."'";
    $sql_bar2.="GROUP BY `Cost_Center`";

}
else{
    $sql_bar1="SELECT COUNT(*) AS ALG, `Cost_Center` FROM `llamadas_perdidas_detallado` WHERE `Cost_Center` IN('VIC TRANS','VIC GESTIO') GROUP BY `Cost_Center`";

    $sql_bar2="SELECT COUNT(*) AS ALG, `Cost_Center` FROM `llamadas_perdidas_detallado` WHERE `Cost_Center` IN('G. LICITAC','GER COMERC','GER CONTRO','GER INFRAE', 
    'GER JURIDI','GER MERCAD','GER NEGOCI','GER SAC','GER TRANSA','GER. FINAN') GROUP BY `Cost_Center`";
}





$rs = $mysqli->query($sql_bar1);
$rs2 = $mysqli->query($sql_bar2);


?>

<script>

	$(document).ready(function () {
        var datos = [];
        var datos2 = [];



        <?php
        $i=0;
        while ($fila1 = $rs->fetch_row()) 
        {
            //-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora              
            echo "\n datos[".$i."] = [];";
            echo "\n datos[".$i."]['num_lla'] = '".$fila1[0]."';";
            echo "\n datos[".$i."]['extension'] = '".$fila1[1]."';";


            $i++;
        }
        $i=0;

        while ($fila2 = $rs2->fetch_row()) 
        {
            //-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora              
            echo "\n datos2[".$i."] = [];";
            echo "\n datos2[".$i."]['num_lla'] = '".$fila2[0]."';";            
            echo "\n datos2[".$i."]['extension'] = '".$fila2[1]."';";


            $i++;
        }

        echo "\n";
        ?>
        var i=0;

        var barData3 = {
            labels: [],
            datasets: [
            {
                label: "",
                backgroundColor: "rgba(227,6,19, 0.7)",
                borderColor: "rgba(227,6,19, 0.7)",
                borderWidth: 1,
                data: []
            },
            {
                label: "",
                backgroundColor: '#1679B0',
                borderColor: "#1679B0",
                borderWidth: 1,
                data: []
            }, 
            
            ]
        };

        var doughnutData = {
          labels: [
          "Break",
          "Baño",
          "Almuerzo",
          "Recepcion",
          "Capacitacion",
          "Tarea",
          "Diario Logon",
          "Diario Logon",
          "Diario Logon",
          "Diario Logon",
          ],
          datasets: [
          {
            data: [],
            borderWidth: 0,
            backgroundColor: [
            "rgba(0,0,255, 1)",
            "rgba(80, 25, 145, 1)",
            "rgba(216, 228, 255, 1)",
            "rgba(255, 115, 0, 1)",
            "rgba(10,200,10, 1)",
            "rgba(255, 220, 20, 1)",
            "rgba(117, 2, 99, 1)",
            "rgba(255, 255, 255, 1)",
            "rgba(22, 121, 176, 0.7)",
            "rgba(227,6,19, 0.8)"

            ],
            hoverBackgroundColor: [
            "#F6A821",
            "#F6A821",
            "#F6A821",
            "#F6A821",
            "#F6A821",
            "#F6A821",
            "#F6A821",
            "#F6A821",
            "#F6A821",
            "#F6A821"

            ]
        }]
    };


    while(datos[i]){

       barData3.labels.push((datos[i]['extension']))       
       barData3.datasets[i]["label"]=(datos[i]['extension'])
       barData3.datasets[i]["data"].push(datos[i]['num_lla'])
       i++;
   }
   i=0;
   while(datos2[i]){
    doughnutData.datasets[0]["data"].push(datos2[i]['num_lla'])
    i++;
}




        /**
         * Options for Line chart
         */

         var globalOptions = {
         	responsive: true,
         	legend: {
         		labels:{
         			fontColor:"#90969D"
         		}
         	},
         	scales: {
         		xAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}],
         		yAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}]
         	}
         };




         var c6 = document.getElementById("PorHblaInPorHblaOutTMO").getContext("2d");
         new Chart(c6, {type: 'bar', data: barData3, options: globalOptions});

         var c1 = document.getElementById("PorHblaInPorHblaOutTMO2").getContext("2d");
         new Chart(c1, {type: 'doughnut', data: doughnutData, options: globalOptions});




     });
 </script>

</body>

</html>