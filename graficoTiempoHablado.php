<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>Auctor | CTI Monitor</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="styles/style.css">

    <!-- Vendor scripts -->
    <script src="vendor/pacejs/pace.min.js"></script>
    <script src="vendor/jquery/dist/jquery.min.js"></script>

    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/chart.js/dist/Chart.min.js"></script>
    <!----DatePicker------->
	<!-- App scripts -->
<script src="scripts/luna.js"></script>

    <style type="text/css">

</style>


</head>
<body>

	<!-- Wrapper-->
	<div class="wrapper">
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    ?>
    <!-- End navigation-->


    <!-- Main content-->
    <section class="content">
    	<div class="container-fluid">


    		<div class="row">
    			<div class="col-lg-12">
    				<div class="view-header">
    					<div class="header-icon">
    						<i class="pe page-header-icon pe-7s-graph3"></i>
    					</div>
    					<div class="header-title">
    						<h3>Tiempo Hablado</h3>
    						<small>
    							Datos Estad&iacute;sticos
    						</small>
    					</div>
    				</div>
    				<hr>
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-12">
    				<div class="header-title">
    					<form action="#" method="post">
						  <p>Desde
                                <input type="date" id="desde" name="desde" autocomplete="off" />
                                Hasta:
                                <input type="date" id="hasta" name="hasta" autocomplete="off"/>
                            </p>

    						<br>
							<input type="submit" class="btn btn-w-md btn-success" value="Cargar Fechas">
    					</form>
    				</div>
    				<hr>
    			</div>
    		</div>

    		<div class="col-md-12">
    			<div class="panel">
    				<div class="panel-body">
    					<div>
    						<canvas id="PorHblaInPorHblaOutTMO" height="180"></canvas>
    					</div>
    				</div>
    			</div>
    		</div>
    		<!-- End main content-->
    	</div>
    </section>

</div>
</body>
<!-- End wrapper-->
<?php
if(isset($_POST['desde']))
{
	$sql_bar1="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`St_Piloto_Llamadas_Atendidas_Antes_5_Segundos`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE `Fecha` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."' GROUP BY `Fecha`";
	$sql_bar2="SELECT `Fecha`, SUM(`Salidas_Locales`) AS Sal_Loc, SUM(`Llegadas_ACD_Salientes_Respondidas`) AS Ll_Acd_Res FROM diario_agente WHERE `Fecha` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."' GROUP BY `Fecha`";
	$sql_bar3="SELECT `Dia`, SUM(`Duracion_Logon`) AS dur_log, SUM(`Duracion_Logon`)/60 AS dur_log_60 FROM diario_sesion WHERE `Dia` BETWEEN '".$_POST['desde']."' AND '".$_POST['hasta']."'";
}
else
{
	$sql_bar1="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`St_Piloto_Llamadas_Atendidas_Antes_5_Segundos`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto GROUP BY `Fecha`";
	$sql_bar2="SELECT `Fecha`, SUM(`Salidas_Locales`) AS Sal_Loc, SUM(`Llegadas_ACD_Salientes_Respondidas`) AS Ll_Acd_Res FROM diario_agente WHERE `Fecha` GROUP BY `Fecha`";
	$sql_bar3="SELECT `Dia`, SUM(`Duracion_Logon`) AS dur_log, SUM(`Duracion_Logon`)*60 AS dur_log_60 FROM diario_sesion WHERE `Dia`";
}

$rs = $mysqli->query($sql_bar1);
$rs2 = $mysqli->query($sql_bar2);
$rs3 = $mysqli->query($sql_bar3);
?>
<script>
	$(document).ready(function () {
        open();
        
		var datos = [];
		var datos2 = [];

		<?php
		$i=0;
		while ($fila1 = $rs->fetch_row()) 
		{
			//-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora				
			$sum_entrant=intval($fila1[1])+intval($fila1[2])+intval($fila1[3]);
			$aten=$sum_entrant-intval($fila1[4]);
			echo "\n datos[".$i."] = [];";
			echo "\n datos[".$i."]['fecha'] = '".$fila1[0]."';";
			echo "\n datos[".$i."]['Ll_ent'] = '".$sum_entrant."';";
			echo "\n datos[".$i."]['Ll_aba'] = '".$fila1[4]."';";
			echo "\n datos[".$i."]['Ll_aten'] = '".$aten."';";
			//--------Llamadas atendidas por agente Cortas - Largas
			$sum_cort=intval($fila1[5])+intval($fila1[6]);
			$ll_largas=$sum_entrant-$sum_cort;
			echo "\n datos[".$i."]['ll_cortas'] = '".$sum_cort."';";
			echo "\n datos[".$i."]['ll_largas'] = '".$ll_largas."';";
			//-------Tiempo Medio de Operación
			echo "\n datos[".$i."]['tm_ttl'] = '".$fila1[5]."';";
			echo "\n datos[".$i."]['tm_prom'] = '".$fila1[6]."';";
			echo "\n datos[".$i."]['tm_ll_con'] = '".$fila1[7]."';";
			$i++;
		}
		$i=0;
		while ($fila2 = $rs2->fetch_row()) 
		{
			//-------Llamadas Salientes Locales y Externas				
			?>
			if(datos['<?php echo $i; ?>']==undefined){
				datos['<?php echo $i; ?>'] = [];
			}
			<?php 
			echo "\n datos[".$i."]['fecha2'] = '".$fila2[0]."';";
			echo "\n datos[".$i."]['Ll_sal'] = '".$fila2[1]."';";
			echo "\n datos[".$i."]['Ll_sal_ext'] = '".$fila2[2]."';";

			$i++;
		}

		$i=0;
		while ($fila3 = $rs3->fetch_row()) 
		{
			echo "\n datos2[".$i."] = [];";
			echo "\n datos2[".$i."]['dur_logon'] = '".$fila3[1]."';";
			echo "\n datos2[".$i."]['dur_logon_60'] = '".$fila3[2]."';";
			$i++;
		}

		echo "\n";
		?>
		var i=0;
		
		var doughnutData2 = {
			labels: [
			"% IN",
			"T.M.O.",
			"% OUT"
			],
			datasets: [
			{
				data: [],
				borderWidth: 0,
				backgroundColor: [
				"rgba(255,0,0,1)",
				"rgba(22, 121, 176, 0.7)",
				"rgba(227,6,19, 0.8)"
				],
				hoverBackgroundColor: [
				"#F6A821",
				"#F6A821",
				"#F6A821"
				]
			}]
		};

		var ttl=0;
		var ttl2=0;
		var ttl3=0;

		while(datos[i]){
		
			ttl=ttl+parseInt(datos[i]['tm_ttl']);
			ttl2=ttl2+parseInt(datos[i]['tm_prom']);
			ttl3=ttl3+parseInt(datos[i]['tm_ll_con']);
			i++;
		}
	
		doughnutData2.datasets[0]["data"].push((ttl/parseInt(datos2[0]['dur_logon'])))
		doughnutData2.datasets[0]["data"].push(ttl2)
		doughnutData2.datasets[0]["data"].push(ttl/ttl3)

        /**
         * Options for Line chart
         */

         var globalOptions = {
         	responsive: true,
         	legend: {
         		labels:{
         			fontColor:"#90969D"
         		}
         	},
         	scales: {
         		xAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}],
         		yAxes: [{
         			ticks: {
         				fontColor: "#90969D"
         			},
         			gridLines: {
         				color: "#37393F"
         			}
         		}]
         	}
         };
        
		var c6 = document.getElementById("PorHblaInPorHblaOutTMO").getContext("2d");
        new Chart(c6, {type: 'bar', data: doughnutData2, options: globalOptions});
     });
</script>

</body>

</html>