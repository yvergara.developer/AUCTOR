<?php
require('db.php');
?>
<!DOCTYPE html>
<html>
<head>
    <?php
    session_start();
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>Auctor | CTI Monitor</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="vendor/datatables/datatables.min.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>

    <!-- Wrapper-->
    <div class="wrapper">

        <!-- Header-->
<!--     <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <div id="mobile-menu">
                    <div class="left-nav-toggle">
                        <a href="#">
                            <i class="stroke-hamburgermenu"></i>
                        </a>
                    </div>
                </div>
                <a class="navbar-brand" href="panelControl.html">
                    Auctor                    <span>v.1.3</span>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div class="left-nav-toggle">
                    <a href="">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class=" profil-link">
                        <a href="panelControl.html">
                            <span class="profile-address">Deysi.Pautt@constructoracolpatria.com</span>
                            <img src="images/profile.jpg" class="img-circle" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav> -->
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    $var = basename(__FILE__);
    if ($var == 'callCenter.php') {
        echo 'class = "active"';
    }
    
    ?>
    <!-- End navigation-->

    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-albums"></i>
                        </div>
                        <div class="header-title">
                            <h3>Call Center</h3>
                            <small>
                                Auctor | CTI Monitor  
                            </small>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
            
            <form action="uploadHtml.php" method="post" enctype="multipart/form-data">
                <input name="fileupload" id="fileupload" type ="file" ><i class="fa fa-upload"></i>    
                <input type="submit" value="Submit">
            </form>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-heading">

                        </div>
                        <div class="panel-body">
                            <p>
                                Hist&oacute;rico
                            </p>
                            <div class="table-responsive">

                                <table id="tableExample3" class="table table-striped table-hover">
                                    <thead>
                                        <tr align='center'>
                                            <th>Nodo</th>
                                            <th>Cont. Llamadas</th>
                                            <th>%</th>
                                            <th># Llam. Susp. Nodo</th>
                                            <th># Llam. Suspendidas</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        
                                        $consulta = "SELECT * FROM TB_ACTIVIDAD";
                                        $resultado = $mysqli->query($consulta);
                                        while ($fila = $resultado->fetch_row()) {
                                            
                                           echo "<tr align='center'>";
                                           echo "<td>$fila[0]";
                                           echo "<td>$fila[1]";
                                           echo "<td>$fila[2]";
                                           echo "<td>$fila[3]";
                                           echo "<td>$fila[4]";
                                           
                                           echo "</tr>";
                                       }
                                       ?>
                                       
                                   </tbody>
                               </table>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>
   <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="vendor/pacejs/pace.min.js"></script>
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/datatables/datatables.min.js"></script>

<!-- App scripts -->
<script src="scripts/luna.js"></script>


<script>
    $(document).ready(function () {
      open();
        
        $('#tableExample3').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            buttons: [

            {extend: 'csv',title: 'ExampleFile', className: 'btn-sm'},
            {extend: 'excelHtml5',title: 'ExampleFile', className: 'btn-sm'},            
            {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
            {extend: 'print',className: 'btn-sm'}
            ]
        });

    });
</script>

</body>

</html>