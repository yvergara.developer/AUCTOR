<?php
require('db.php');
?>
<!DOCTYPE html>
<html>
<head>
	<?php
	session_start();
	?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

	<!-- Page title -->
	<title>Auctor | CTI Monitor</title>

	<!-- Vendor styles -->
	<link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
	<link rel="stylesheet" href="vendor/animate.css/animate.css"/>
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>
	<link rel="stylesheet" href="vendor/datatables/datatables.min.css"/>

	<!-- App styles -->
	<link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
	<link rel="stylesheet" href="styles/pe-icons/helper.css"/>
	<link rel="stylesheet" href="styles/stroke-icons/style.css"/>
	<link rel="stylesheet" href="styles/style.css">
</head>
<body>

	<!-- Wrapper-->
	<div class="wrapper">
	
    <?php
    include("cabecera.php");
    ?>
    <!-- End header-->

    <!-- Navigation-->
    <?php
    include("menu.php");
    $var = basename(__FILE__);
    if ($var == 'ivr.php') {
    	echo 'class = "active"';
    }
    
    ?>
    <!-- End navigation-->

    <!-- Main content-->
    <section class="content">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-12">
    				<div class="view-header">
    					<div class="header-icon">
    						<i class="pe page-header-icon pe-7s-call"></i>
    					</div>
    					<div class="header-title">
    						<h3>Llamadas Internas - Administrativo</h3>
    						<small>
    							Registro de Actividad | Auctor CTI Monitor
    						</small>
    					</div>
    				</div>
    				<hr>
    			</div>
    		</div>

    		<div class="col-lg-4">
    			<form action="upload.php?var1=4" method="post" enctype="multipart/form-data">
    				<label class="btn btn-accent" for="fileupload">
    					<input name="fileupload" id="fileupload" type="file" style="display:none;"
    					onchange="$('#upload-file-info2').html($(this).val());">
    					<span id="upload-file-info2">Seleccionar Archivo</span>
    				</label>
    				<input type="submit" class="btn btn-w-md btn-success" value="Cargar">
    			</form>
    		</div>

    		<div class="row">
    			<div class="col-md-12">
    				<div class="panel panel-filled">
    					<div class="panel-heading">

    					</div>
    					<div class="panel-body">
    						<div class="table-responsive">

    							<table id="tableExample3" class="table table-striped table-hover">
    								<thead>
    									<tr align='center'>
    										<th>Filtro</th>
    										<th>Centro Costo</th>
    										<th>Nombre</th>
    										<th>Extensi&oacute;n</th>
    										<th>Fecha / Hora</th>
    										<th>Num. Llamado</th>
    										<th>Duraci&oacute;n</th>
    									</tr>
    								</thead>
    								<tbody>
    									<?php
    									$i=0;
    									$consulta = "SELECT * FROM llamadas_internas";
    									$resultado = $mysqli->query($consulta);
    									while ($fila = $resultado->fetch_row()) {


    										echo "<tr align='center'>";
    										echo "<td class='dele'>$fila[1]";
    										echo "<td>$fila[2]";
    										echo "<td>$fila[3]";
    										echo "<td>$fila[4]";
    										echo "<td>$fila[5]";
    										echo "<td>$fila[6]";
    										echo "<td>$fila[7]";
    										echo "</tr>";
    										
                                  }
                                  ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="vendor/pacejs/pace.min.js"></script>
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/datatables/datatables.min.js"></script>

<!-- App scripts -->
<script src="scripts/luna.js"></script>


<script>
	var x = document.getElementsByClassName("dele");
	console.log(x);
	$(document).ready(function () {

		open();

		$('#tableExample3').DataTable({
			dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
			"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
			buttons: [

			{extend: 'csv',title: 'llamadasInternas', className: 'btn-sm'},
			{extend: 'excelHtml5',title: 'llamadasInternas', className: 'btn-sm'},
			{extend: 'pdf', title: 'llamadasInternas', className: 'btn-sm'},
			{extend: 'print',className: 'btn-sm'}
			]
		});

	});
</script>

</body>

</html>