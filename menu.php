<?php
include 'db.php';
?>

<aside class="navigation">
    <nav>
        <ul class="nav luna-nav">

            <li class="nav-category">
                Monitoreo
            </li>

            <li class="inactive" id=1>
                <a href="panelControl.php">Panel de Control</a>
            </li>

            <li class="nav-category">
                Registro / Reporte
            </li>

            <li >
                <a id="tb1" href="#tables1" data-toggle="collapse" aria-expanded="false">
                    Administrativo<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="tables1" class="nav nav-second collapse">
                    <li id="llamadasInternas"><a href="llamadasInternas.php">Llamadas Internas</a></li>
                    <li id="llamadasEntrantes"><a href="llamadasEntrantes.php">Llamadas Entrantes</a></li>
                    <li id="llamadasPerdidasDet"><a href="llamadasPerdidasDet.php">Llamadas Perdidas Detalle</a></li>
                    <li id="llamadasPerdidasTot"><a href="llamadasPerdidasTot.php">Llamadas Perdidas Total</a></li>
                    <li id="llamadasSalientes"><a href="llamadasSalientes.php">Llamadas Salientes</a></li>
                    <li id="grupoTroncal"><a href="grupoTroncal.php">Grupo Troncal</a></li>
                    <li id="directorio"><a href="directorio.php">Directorio</a></li>
                </ul>
            </li>

            <li>
                <a id="tb2" href="#tables2" data-toggle="collapse" aria-expanded="false">
                    CCS - Macro<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>

                <ul id="tables2" class="nav nav-second collapse">                        
                    <li id="agentes"><a href="agentes.php">Agentes</a></li>
                    <li id="pilotos"><a href="pilotos.php">Pilotos</a></li>
                    <li id="sesiones"><a href="sesiones.php">Sesiones</a></li>
                </ul>
            </li>

            <li>
                <a id="tb4" href="#tables4" data-toggle="collapse" aria-expanded="false">
                    Gateway Celular<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="tables4" class="nav nav-second collapse">                        
                    <li id="llamadasGateway"><a href="llamadasGateway.php">Callog</a></li>
                </ul>
            </li>

            <li>
                <a id="tb5" href="#tables5" data-toggle="collapse" aria-expanded="false">
                    I.V.R.<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="tables5" class="nav nav-second collapse">
                    <li id="aaReport"><a href="aaReport.php">AA - Report</a></li>
                </ul>
            </li>

            <li class="nav-category">
                An&aacute;lisis
            </li>

            <li>
                <a id="tb3" href="#tables3" data-toggle="collapse" aria-expanded="false">
                    Gr&aacute;ficos<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="tables3" class="nav nav-second collapse">
                    <li id="graficoLlamAbandonadas"><a href="graficoLlamAbandonadas.php">Llamadas Abandonadas, Entrantes y Atendidas</a></li>
                    <li id="graficoLlamAtendidasAgente"><a href="graficoLlamAtendidasAgente.php">Llamadas Atendidas por Agente (Cortas - Largas) </a></li>
                    <li id="graficoLlamSalientes"><a href="graficoLlamSalientes.php">Llamadas Salientes Locales y Externas</a></li>
                    <li id="graficoLlamTiempoMedio"><a href="graficoLlamTiempoMedio.php">Tiempo Medio de Operación (T.M.O.)</a></li>
                    <li id="graficoLlamTiempoConectado"><a href="graficoLlamTiempoConectado.php">Porcentaje A.D.H. Sesiones y Cuartilamiento</a></li>
                    <li id="graficoTiempoHablado"><a href="graficoTiempoHablado.php">Tiempo Hablado IN / Tiempo Hablado Out / TMO</a></li>
                    <li id="grafico10NoContestan"><a href="grafico10NoContestan.php">Top 10 Ext que No contestan</a></li>
                    <li id="graficoLlamadasPerdidasPorVicepresidencia"><a href="graficoLlamadasPerdidasPorVicepresidencia.php">Llamadas Perdidas Vicepresidencia </a></li>
                    <li id="graficoEstadisticoGerencia"><a href="graficoEstadisticoGerencia.php">Informe Estad&iacute;stico Gerencia</a></li>
                    <li id="graficoContadorLlamadasOpcionIVR"><a href="graficoContadorLlamadasOpcionIVR.php">Reporte Contador de Llamadas / Opci&oacute;n I.V.R.</a></li>
                    <li id="graficoLlamadasGatewayCelulares"><a href="graficoLlamadasGatewayCelulares.php">Llamadas Gateway Celular</a></li>



                </ul>
            </li>

            <li class="nav-info">
                <div class="m-t-xs">
                    <span class="c-white">Auctor</span> | CTI Monitor.
                </div>
            </li>
        </ul>
    </nav>
</aside>';

<script type="text/javascript">
    function open(){
        var name=window.location.pathname;
        name=name.replace('/','');
        name=name.replace('.php','');
      //  console.log(name);
      var idpar=document.getElementById(name).parentElement.id;
      switch(idpar) {
        case "tables1":
        document.getElementById("tb1").click();
        break;
        case "tables2":
        document.getElementById("tb2").click();
        break;
        case "tables3":
        document.getElementById("tb3").click();
        break;
        case "tables4":
        document.getElementById("tb4").click();
        break;
        case "tables5":
        document.getElementById("tb5").click();
        break;
    }
       // console.log(idpar);

   }

</script>

<?php
