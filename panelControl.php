<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>Auctor | CTI Monitor</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="vendor/toastr/toastr.min.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="styles/style.css"> 
</head>
<body>

    <!-- Wrapper-->
    <div class="wrapper">

        <?php
        include("cabecera.php");
        include("menu.php");
        ?>
        <!-- End navigation-->

        <?php
    // ------ SAC

        $sql_bar1="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`Llamadas_Atendidas_Sin_Espera`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE Codigo_Piloto=700";

        $rs = $mysqli->query($sql_bar1);
        $fila1 = $rs->fetch_row();
        $sum1=$fila1[1]+$fila1[2]+$fila1[3];
        $tot_ab1=$fila1[4];
        $sin_espera1=$fila1[5];
        $div1=($sum1-$tot_ab1)/$sum1;
        $div1=round($div1, 2);
        $div1_2=($sin_espera1)/$sum1;
        $div1_2=round($div1_2, 2);
    //-----Canal

        $sql_bar2="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`Llamadas_Atendidas_Sin_Espera`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE Codigo_Piloto=701";

        $rs2 = $mysqli->query($sql_bar2);

        $fila2 = $rs2->fetch_row();
        $sum2=$fila2[1]+$fila2[2]+$fila2[3];
        $tot_ab2=$fila2[4];
        $sin_espera2=$fila2[5];
        $div2=($sum2-$tot_ab2)/$sum2;
        $div2=round($div2, 2);
        $div2_2=($sin_espera2)/$sum2;
        $div2_2=round($div2_2, 2);
    //-----Canal

        $sql_bar3="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`Llamadas_Atendidas_Sin_Espera`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE Codigo_Piloto=704";

        $rs3 = $mysqli->query($sql_bar3);

        $fila3 = $rs3->fetch_row();

        $sum3=$fila3[1]+$fila3[2]+$fila3[3];
        $tot_ab3=$fila3[4];
        $sin_espera3=$fila3[5];

        $div3=($sum3-$tot_ab3)/$sum3;
        $div3=round($div3, 2);

        $div3_2=($sin_espera3)/$sum3;
        $div3_2=round($div3_2, 2);

        ?>
        <!-- Main content-->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="view-header">
                            <div class="pull-right text-right" style="line-height: 14px">
                            </div>
                            <div class="header-icon">
                                <i class="pe page-header-icon pe-7s-monitor"></i>
                            </div>
                            <div class="header-title">
                                <h3 class="m-b-xs">Panel de Control</h3>
                                <small>
                                    Datos generales acerca de la operación del Call Center.
                                </small>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <!-- Primera Caja -->


                    <div class="col-lg-2 col-xs-6">
                        <div class="panel panel-filled">

                            <div class="panel-body">
                                <h2 class="m-b-none">
                                    <?php echo $sum1; ?> <span class="slight"> </i> <?php echo $div1; ?> % </span>
                                </h2>
                                <div class="slight m-t-sm"></i> Nivel de Atenci&oacute;n <span class="c-white">S.A.C.</span>  </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-6">
                        <div class="panel panel-filled">
                          <div class="panel-body">
                            <h2 class="m-b-none">
                                <?php echo $sum2; ?> <span class="slight"> </i> <?php echo $div2; ?> % </span>
                            </h2>
                            <div class="slight m-t-sm"></i> Nivel de Atenci&oacute;n <span class="c-white">Canal Digital</span>  </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-6">
                    <div class="panel panel-filled">

                        <div class="panel-body">
                            <h2 class="m-b-none">
                                <?php echo $sum3; ?> <span class="slight"> </i> <?php echo $div3; ?> % </span>
                            </h2>
                            <div class="slight m-t-sm"></i> Nivel de Atenci&oacute;n <span class="c-white">Conmutador</span>  </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-filled">
                        <div class="row">
                            <div class="col-md-4">

                                <div class="panel-body h-200 list">
                                    <div class="stats-title pull-left">
                                        <h4>Nivel de Atenci&oacute;n S.A.C.</h4>
                                    </div>
                                    <div class="m-t-xl">
                                        <small>
                                            El <?php echo $div1; ?> informaci&oacute;n relevante.
                                        </small>
                                    </div>
                                    <div class="progress m-t-xs full progress-small">
                                        <div style="width: 35%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar" class=" progress-bar progress-bar-warning">
                                            <span class="sr-only"><?php echo $div1; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="panel-body">
                                    <div class="text-center slight">
                                    </div>

                                    <div class="flot-chart" style="height: 160px;margin-top: 5px">
                                        <div class="flot-chart-content" id="flot-line-chart"></div>
                                    </div>

                                    <div class="small text-center"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!-- Sin Espera-->


            <div class="row">
             <div class="col-lg-2 col-xs-6">
                <div class="panel panel-filled">

                    <div class="panel-body">
                        <h2 class="m-b-none">
                            <?php echo $sin_espera1; ?> <span class="slight"> </i> <?php echo $div1_2; ?> % </span>
                        </h2>
                        <div class="slight m-t-sm"></i> Nivel de Servicio <span class="c-white">S.A.C.</span>  </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <div class="panel panel-filled">
                  <div class="panel-body">
                    <h2 class="m-b-none">
                        <?php echo $sin_espera2; ?> <span class="slight"> </i> <?php echo $div2_2; ?> % </span>
                    </h2>
                    <div class="slight m-t-sm"></i> Nivel de Servicio <span class="c-white">Canal Digital</span>  </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-xs-6">
            <div class="panel panel-filled">

                <div class="panel-body">
                    <h2 class="m-b-none">
                        <?php echo $sin_espera3; ?> <span class="slight"> </i> <?php echo $div3_2; ?> % </span>
                    </h2>
                    <div class="slight m-t-sm"></i> Nivel de Servicio <span class="c-white">Conmutador</span>  </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-filled">
                <div class="row">
                    <div class="col-md-4">

                        <div class="panel-body h-200 list">
                            <div class="stats-title pull-left">
                                <h4>Nivel de Servicio S.A.C.</h4>
                            </div>
                            <div class="m-t-xl">
                                <small>
                                    El <?php echo $div1_2; ?> informaci&oacute;n relevante.
                                </small>
                            </div>


                            <div class="progress m-t-xs full progress-small">
                                <div style="width: 35%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar" class=" progress-bar progress-bar-warning">
                                    <span class="sr-only"><?php echo $div1; ?></span>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel-body">
                            <div class="text-center slight">
                            </div>

                            <div class="flot-chart" style="height: 160px;margin-top: 5px">
                                <div class="flot-chart-content" id="flot-line-chart2"></div>
                            </div>

                            <div class="small text-center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table  class="table">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Extensi&oacute;n</th>
                                    <th>&Aacute;rea</th>
                                    <th>Minutos Conexión</th>
                                    <th>Llamadas Entrantes</th>
                                    <th>Porcentaje Atenci&oacute;n</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=0;
                                $consulta = "SELECT diario_sesion.Nombre_Agente, diario_sesion.Numero_Directorio, diario_sesion.Nombre_Piloto, diario_sesion.Duracion_Logon, diario_agente.Timbres_ACD, diario_agente.Llamadas_ACD_Atendidas_Sin_Codigo_Transaccion FROM diario_sesion, diario_agente WHERE diario_sesion.Numero_Directorio= diario_agente.Agente_Numero AND diario_sesion.Dia = diario_agente.Fecha LIMIT 7";
                                $resultado = $mysqli->query($consulta);
                                $rs = $mysqli->query($consulta);
                                $t=0;
                                while ($fila = $resultado->fetch_row()) {
                                    if($fila[5]==0){
                                        $divi=0;
                                    }
                                    else{
                                        $divi= $fila[4] / $fila[5];
                                    }
                                    echo "<tr align='center'>";
                                    echo "<td>$fila[0]";
                                    echo "<td>$fila[1]";
                                    echo "<td>$fila[2]";
                                    echo "<td>$fila[3]";
                                    echo "<td>$fila[4]";
                                    echo "<td><div class='sparkline$t'></div></td>";
                                    echo "</tr>";  
                                    $t++;                                 
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">

            <div class="panel panel-b-accent m-t-sm">
                <div class="panel-body text-center p-m">
                    <h2 class="font-light">
                        Porcentaje Atenci&oacute;n
                    </h2>
                    
                    <br/>
                    <div class="sparklin m-t-xs"></div>
                </div>
            </div>
        </div>

    </div>

</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="vendor/pacejs/pace.min.js"></script>
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/toastr/toastr.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/flot/jquery.flot.min.js"></script>
<script src="vendor/flot/jquery.flot.resize.min.js"></script>
<script src="vendor/flot/jquery.flot.spline.js"></script>

<!-- App scripts -->
<script src="scripts/luna.js"></script>

<script>
    $(document).ready(function () {

       <?php
       $i=0;
       $ar='';
       while ($fila1 = $rs->fetch_row()) 
       {
            //-------Llamadas abandonadas, entrantes y atendidas por intervalo de hora              
        echo "$('.sparkline".$i."').sparkline([".$fila1[4].", ".$fila1[5]."], {
            type: 'pie',
            sliceColors: ['#E30613', '#404652']
        });";

        if($fila1[5]==0){
            $divid=0;
        }
        else{
            $divid= $fila1[4] / $fila1[5];
        }

        //------

        if($i==0){
            $ar.=$divid;
        }
        else{
            $ar=(string)$ar .','.(string)$divid;
        }

        $i++;
    }

    echo "$('.sparklin').sparkline([$ar], {
        type: 'line',
        lineColor: '#FFFFFF',
        lineWidth: 3,
        fillColor: '#9C1006',
        height: 75,
        width: '100%'
    });";

    ?>
    


        // Flot charts data and options
        var data1 = [ [0, 16], [1, 24], [2, 11], [3, 7], [4, 10], [5, 15], [6, 24], [7, 30] ];
        var data2 = [ [0, 26], [1, 44], [2, 31], [3, 27], [4, 36], [5, 46], [6, 56], [7, 66] ];

        var data3 = [ [0, 15], [1, 20], [2, 19], [3, 9], [4, 17], [5, 35], [6, 44], [7, 39] ];
        var data4 = [ [0, 16], [1, 47], [2, 12], [3, 12], [4, 22], [5, 19], [6, 56], [7, 36] ];

        var chartUsersOptions = {
            series: {
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 1
                }
            },
            grid: {
                tickColor: "#404652",
                borderWidth: 0,
                borderColor: '#404652',
                color: '#404652'
            },
            colors: [ "#E30613","#9C1006"]
        };

        $.plot($("#flot-line-chart"), [data2, data1], chartUsersOptions);
        $.plot($("#flot-line-chart2"), [data3, data4], chartUsersOptions);
    });
</script>

</body>

</html>