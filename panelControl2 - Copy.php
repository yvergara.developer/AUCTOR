<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>Auctor | CTI Monitor</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="vendor/animate.css/animate.css"/>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="vendor/toastr/toastr.min.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="styles/style.css"> 
</head>
<body>

    <!-- Wrapper-->
    <div class="wrapper">
    
    <?php
    include("cabecera.php");
    include("menu.php");
    ?>
    <!-- End navigation-->

    <?php
    // ------ SAC

    $sql_bar1="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`Llamadas_Atendidas_Sin_Espera`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE Codigo_Piloto=700";

    $rs = $mysqli->query($sql_bar1);
    $fila1 = $rs->fetch_row();
    $sum1=$fila1[1]+$fila1[2]+$fila1[3];
    $tot_ab1=$fila1[4];
    $sin_espera1=$fila1[5];
    $div1=($sum1-$tot_ab1)/$sum1;
    $div1=round($div1, 2);
    $div1_2=($sin_espera1)/$sum1;
    $div1_2=round($div1_2, 2);
    //-----Canal

    $sql_bar2="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`Llamadas_Atendidas_Sin_Espera`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE Codigo_Piloto=701";

    $rs2 = $mysqli->query($sql_bar2);

    $fila2 = $rs2->fetch_row();
    $sum2=$fila2[1]+$fila2[2]+$fila2[3];
    $tot_ab2=$fila2[4];
    $sin_espera2=$fila2[5];
    $div2=($sum2-$tot_ab2)/$sum2;
    $div2=round($div2, 2);
    $div2_2=($sin_espera2)/$sum2;
    $div2_2=round($div2_2, 2);
    //-----Canal

    $sql_bar3="SELECT `Fecha`, SUM(`Llamadas_Recibidas_Estado_Abierto`) AS Ll_res, SUM(`Llamadas_Recibidas_Estado_Bloqueado`) AS Ll_bloc, SUM(`Llamadas_Recibidas_Estado_Desvio_General`) AS Ll_des, SUM(`Total_Abandonos`) as Ab, SUM(`Llamadas_Atendidas_Sin_Espera`) as Ac, SUM(`St_Piloto_Llamadas_Atendidas_Antes_15_Segundos`) As Ad , SUM(`Tiempo_Total_Tratamiento_Llamada`) As Ae , SUM(`Tiempo_Medio_Tratamiento_Llamada`) As Af,  SUM(`Tiempo_Total_Llamadas_Consulta`) AS Ag  FROM diario_piloto WHERE Codigo_Piloto=704";

    $rs3 = $mysqli->query($sql_bar3);

    $fila3 = $rs3->fetch_row();

    $sum3=$fila3[1]+$fila3[2]+$fila3[3];
    $tot_ab3=$fila3[4];
    $sin_espera3=$fila3[5];

    $div3=($sum3-$tot_ab3)/$sum3;
    $div3=round($div3, 2);

    $div3_2=($sin_espera3)/$sum3;
    $div3_2=round($div3_2, 2);

    ?>
    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="view-header">
                        <div class="pull-right text-right" style="line-height: 14px">
                        </div>
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-monitor"></i>
                        </div>
                        <div class="header-title">
                            <h3 class="m-b-xs">Panel de Control</h3>
                            <small>
                                Datos generales acerca de la operación del Call Center.
                            </small>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>

            <div class="row">
                <!-- Primera Caja -->


                <div class="col-lg-2 col-xs-6">
                    <div class="panel panel-filled">

                        <div class="panel-body">
                            <h2 class="m-b-none">
                                <?php echo $sum1; ?> <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i> <?php echo $div1; ?> </span>
                            </h2>
                            <div class="small">Nivel de Atención SAC PILOTOS </div>
                            <div class="slight m-t-sm"></i> Nivel de Atenci&oacute;n <span class="c-white">Pilotos</span>  </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-6">
                    <div class="panel panel-filled">
                      <div class="panel-body">
                        <h2 class="m-b-none">
                            <?php echo $sum2; ?> <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i> <?php echo $div2; ?> </span>
                        </h2>
                        <div class="small">Nivel de Atención Canal Digital PILOTOS</div>
                        <div class="slight m-t-sm"></i> Nivel de Atenci&oacute;n <span class="c-white">Canal Digitel</span>  </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <div class="panel panel-filled">

                    <div class="panel-body">
                        <h2 class="m-b-none">
                            <?php echo $sum3; ?> <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i> <?php echo $div3; ?> </span>
                        </h2>
                        <div class="small">Nivel de Atención Conmutador PILOTOS</div>
                        <div class="slight m-t-sm"></i> Nivel de Atenci&oacute;n <span class="c-white">Conmutador</span>  </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-filled">
                    <div class="row">
                        <div class="col-md-4">

                            <div class="panel-body h-200 list">
                                <div class="stats-title pull-left">
                                    <h4>Nivel de Atencion SAC</h4>
                                </div>
                                <div class="m-t-xl">
                                    <small>
                                        El <?php echo $div1; ?> informaci&oacute;n relevante.
                                    </small>
                                </div>


                                <div class="progress m-t-xs full progress-small">
                                    <div style="width: 35%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar" class=" progress-bar progress-bar-warning">
                                        <span class="sr-only"><?php echo $div1; ?></span>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="panel-body">
                                <div class="text-center slight">
                                </div>

                                <div class="flot-chart" style="height: 160px;margin-top: 5px">
                                    <div class="flot-chart-content" id="flot-line-chart"></div>
                                </div>

                                <div class="small text-center">Eje / Llamadas atendidas / Usuaros activos.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <!-- Sin Espera-->


        <div class="row">
         <div class="col-lg-2 col-xs-6">
            <div class="panel panel-filled">

                <div class="panel-body">
                    <h2 class="m-b-none">
                        <?php echo $sin_espera1; ?> <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i> <?php echo $div1_2; ?> </span>
                    </h2>
                    <div class="small">Nivel de Atención SAC PILOTOS </div>
                    <div class="slight m-t-sm"></i> Nivel de Servicio <span class="c-white">Pilotos</span>  </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-xs-6">
            <div class="panel panel-filled">
              <div class="panel-body">
                <h2 class="m-b-none">
                    <?php echo $sin_espera2; ?> <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i> <?php echo $div2_2; ?> </span>
                </h2>
                <div class="small">Nivel de Atención Canal Digital PILOTOS</div>
                <div class="slight m-t-sm"></i> Nivel de Servicio <span class="c-white">Canal Digitel</span>  </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-xs-6">
        <div class="panel panel-filled">

            <div class="panel-body">
                <h2 class="m-b-none">
                    <?php echo $sin_espera3; ?> <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i> <?php echo $div3_2; ?> </span>
                </h2>
                <div class="small">Nivel de Atención Conmutador PILOTOS</div>
                <div class="slight m-t-sm"></i> Nivel de Servicio <span class="c-white">Conmutador</span>  </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-filled">
            <div class="row">
                <div class="col-md-4">

                    <div class="panel-body h-200 list">
                        <div class="stats-title pull-left">
                            <h4>Nivel de Atencion SAC</h4>
                        </div>
                        <div class="m-t-xl">
                            <small>
                                El <?php echo $div1; ?> informaci&oacute;n relevante.
                            </small>
                        </div>


                        <div class="progress m-t-xs full progress-small">
                            <div style="width: 35%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar" class=" progress-bar progress-bar-warning">
                                <span class="sr-only"><?php echo $div1; ?></span>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel-body">
                        <div class="text-center slight">
                        </div>

                        <div class="flot-chart" style="height: 160px;margin-top: 5px">
                            <div class="flot-chart-content" id="flot-line-chart"></div>
                        </div>

                        <div class="small text-center">Eje / Llamadas atendidas / Usuaros activos.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
<div class="row">
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-body">
                <div class="table-responsive">
                    <table  class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Extensi&oacute;n</th>
                                <th>&Aacute;rea</th>
                                <th>% No contest.</th>
                                <th>Tipo</th>
                                <th>Detalles</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Alejandro Portilla</td>
                                <td>13 33</td>
                                <td>SAC</td>
                                <td><div class="sparkline8"></div> </td>
                                <td>Info</td>
                                <td><a href="#" class="btn btn-default btn-xs">Ver</a></td>
                            </tr>
                            <tr>
                                <td>Laura Torres</td>
                                <td>29 09</td>
                                <td>SAC</td>
                                <td><div class="sparkline10"></div></td>
                                <td>Info</td>
                                <td><a href="#" class="btn btn-default btn-xs active">Ver</a></td>
                            </tr>
                            <tr>
                                <td>Ramiro L&oacute;pez</td>
                                <td>76 27</td>
                                <td>SAC</td>
                                <td><div class="sparkline11"></div></td>
                                <td>Info</td>
                                <td><a href="#" class="btn btn-default btn-xs">Ver</a></td>
                            </tr>
                            <tr>
                                <td>Melissa C&acute;rdenas</td>
                                <td>78 65</td>
                                <td>SAC</td>
                                <td><div class="sparkline12"></div></td>
                                <td>Info</td>
                                <td><a href="#" class="btn btn-default btn-xs">Ver</a></td>
                            </tr>
                            <tr>
                                <td>Tatiana Beltr&aacute;n</td>
                                <td>43 81</td>
                                <td>SAC</td>
                                <td><div class="sparkline13"></div></td>
                                <td>Info</td>
                                <td><a href="#" class="btn btn-default btn-xs">Ver</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">

        <div class="panel panel-b-accent m-t-sm">
            <div class="panel-body text-center p-m">
                <h2 class="font-light">
                    Detalles relevantes a mostrar
                </h2>
                <small>Informaci&oacute;n de detalle</small>
                <br/>
                120,312 <span class="slight"><i class="fa fa-play fa-rotate-270 c-white"> </i> 34%</span>
                <div class="sparkline7 m-t-xs"></div>
            </div>
        </div>
    </div>

</div>

</div>
</section>
<!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="vendor/pacejs/pace.min.js"></script>
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/toastr/toastr.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/flot/jquery.flot.min.js"></script>
<script src="vendor/flot/jquery.flot.resize.min.js"></script>
<script src="vendor/flot/jquery.flot.spline.js"></script>

<!-- App scripts -->
<script src="scripts/luna.js"></script>

<script>
    $(document).ready(function () {


        // Sparkline charts
        var sparklineCharts = function () {
            $(".sparkline").sparkline([20, 34, 43, 43, 35, 44, 32, 44, 52, 45], {
                type: 'line',
                lineColor: '#FFFFFF',
                lineWidth: 3,
                fillColor: '#404652',
                height: 47,
                width: '100%'
            });

            $(".sparkline7").sparkline([10, 34, 13, 33, 35, 24, 32, 24, 52, 35], {
                type: 'line',
                lineColor: '#FFFFFF',
                lineWidth: 3,
                fillColor: '#9C1006',
                height: 75,
                width: '100%'
            });

            $(".sparkline1").sparkline([0, 6, 8, 3, 2, 4, 3, 4, 9, 5, 3, 4, 4, 5, 1, 6, 7, 15, 6, 4, 0], {
                type: 'line',
                lineColor: '#E30613',
                lineWidth: 3,
                fillColor: '#E30613',
                height: 170,
                width: '100%'
            });

            $(".sparkline3").sparkline([-8, 2, 4, 3, 5, 4, 3, 5, 5, 6, 3, 9, 7, 3, 5, 6, 9, 5, 6, 7, 2, 3, 9, 6, 6, 7, 8, 10, 15, 16, 17, 15], {

                type: 'line',
                lineColor: '#fff',
                lineWidth: 3,
                fillColor: '#393b45',
                height: 70,
                width: '100%'
            });

            $(".sparkline5").sparkline([0, 6, 8, 3, 2, 4, 3, 4, 9, 5, 3, 4, 4, 5], {
                type: 'line',
                lineColor: '#E30613',
                lineWidth: 2,
                fillColor: '#E30613',
                height: 20,
                width: '100%'
            });
            $(".sparkline6").sparkline([0, 1, 4, 2, 2, 4, 1, 4, 3, 2, 3, 4, 4, 2, 4, 2, 1, 3], {
                type: 'bar',
                barColor: '#E30613',
                height: 20,
                width: '100%'
            });

            $(".sparkline8").sparkline([4, 2], {
                type: 'pie',
                sliceColors: ['#E30613', '#404652']
            });
            $(".sparkline9").sparkline([3, 2], {
                type: 'pie',
                sliceColors: ['#E30613', '#404652']
            });
            $(".sparkline10").sparkline([4, 1], {
                type: 'pie',
                sliceColors: ['#E30613', '#404652']
            });
            $(".sparkline11").sparkline([1, 3], {
                type: 'pie',
                sliceColors: ['#E30613', '#404652']
            });
            $(".sparkline12").sparkline([3, 5], {
                type: 'pie',
                sliceColors: ['#E30613', '#404652']
            });
            $(".sparkline13").sparkline([6, 2], {
                type: 'pie',
                sliceColors: ['#E30613', '#404652']
            });
        };

        var sparkResize;

        // Resize sparkline charts on window resize
        $(window).resize(function () {
            clearTimeout(sparkResize);
            sparkResize = setTimeout(sparklineCharts, 100);
        });

        // Run sparkline
        sparklineCharts();


        // Flot charts data and options
        var data1 = [ [0, 16], [1, 24], [2, 11], [3, 7], [4, 10], [5, 15], [6, 24], [7, 30] ];
        var data2 = [ [0, 26], [1, 44], [2, 31], [3, 27], [4, 36], [5, 46], [6, 56], [7, 66] ];

        var chartUsersOptions = {
            series: {
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 1

                }

            },
            grid: {
                tickColor: "#404652",
                borderWidth: 0,
                borderColor: '#404652',
                color: '#404652'
            },
            colors: [ "#E30613","#9C1006"]
        };

        $.plot($("#flot-line-chart"), [data2, data1], chartUsersOptions);
    });
</script>

</body>

</html>