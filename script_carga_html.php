<?php

// Motrar todos los errores de PHP
error_reporting(-1);

// No mostrar los errores de PHP
error_reporting(0);

// Motrar todos los errores de PHP
error_reporting(E_ALL);
// Conexión a Base de Datos
require ('db.php');

// Inclusión Librería simple_html_dom.php
include ("simple_html_dom.php");

// Almacenamiento HTML en $html
session_start();
$host  = $_SERVER['HTTP_HOST'];
$html = file_get_html(__DIR__ .'/upload/'.$_SESSION['filename']);
$contenido_reporte = file(__DIR__ .'/upload/'.$_SESSION['filename']);

echo $html;
// Búsqueda de bloques con clase = container en $html
$element = $html->find('.container');

// Declaración e inicialización de arreglos
$dato_string = array();
$numero_llamadas_arbol = array();
$registro_actividad_arbol = array();
$registro_causa_desconexion = array();
$registro_ubicacion_desconexion = array();
$arboles = array();
$num = array();

// Declaración e inicialización de variables
$contActividadArbol = NULL;
$contNumLlamadasArbol = NULL;
$contCausaDesconexion = NULL;
$contUbicacionDesconexion = NULL;
$var_inicio = NULL;
$numero_total_llamadas_todos_arboles = NULL;
$numero_arbol = NULL;
$fecha_inicio_reporte = NULL;
$hora_inicio_reporte = NULL;
$fecha_fin_reporte = NULL;
$hora_fin_reporte = NULL;
$fecha_inicio_datos = NULL;
$hora_inicio_datos = NULL;
$numero_referencia_inicio = NULL;
$fecha_fin_datos = NULL;
$hora_fin_datos = NULL;
$numero_referencia_fin = NULL;

// Lógica para fecha y hora de generación reporte
$contador = 0;
foreach ($contenido_reporte as $num_linea)
{
    $contador++;
    if ($contador == 19)
    {
        $fecha_inicio_reporte = substr("$num_linea",25,-42); // Fecha inicio reporte
        $hora_inicio_reporte = substr("$num_linea",35,-33); // Hora inicio reporte
        $fecha_fin_reporte = substr("$num_linea",51,-16); // Fecha fin reporte
        $hora_fin_reporte = substr("$num_linea",60,-7); // Hora fin reporte                
    }
    elseif ($contador == 20) 
    {
        $fecha_inicio_datos = substr("$num_linea",20,-24); // Fecha inicio reporte
        $hora_inicio_datos = substr("$num_linea",32,-15); // Hora inicio reporte
        $numero_referencia_inicio = substr("$num_linea",41,-6); // Fecha fin reporte

    }
    elseif ($contador == 21) 
    {
        $fecha_fin_datos = substr("$num_linea",18,-24); // Fecha inicio reporte
        $hora_fin_datos = substr("$num_linea",30,-15); // Hora inicio reporte
        $numero_referencia_fin = substr("$num_linea",39,-6); // Fecha fin reporte
    }
}

// Lógica para cada contenedor
foreach ($element as $contenedor)
{
    // Contenedor - Títulos    
    // Dato - Contenedor: Número total de llamadas en todos los árboles (Total number of calls for all trees : XXX)
    if (strstr($contenedor,"<h3>Total number of calls for all trees :"))
    {
        foreach($contenedor->find('h3') as $li)
        {
            $dato = strip_tags($li);
            
            // Depuración de variable (Eliminación de caracteres NO numéricos)
            for($contNumLlamadasTodosArboles = 0; $contNumLlamadasTodosArboles < strlen($dato); $contNumLlamadasTodosArboles++)
            {
                if(is_numeric($dato[$contNumLlamadasTodosArboles]))
                {
                    $dato_string[$contNumLlamadasTodosArboles] = $dato[$contNumLlamadasTodosArboles];
                }
            }
            $numero_total_llamadas_todos_arboles = implode("",$dato_string);
        }
        //OK
    }

    // Datos:  Tabla Numero de llamadas por árbol (tree / number of calls / mean duration)
    //Contenedor - Número llamadas por árbol
    else if (strstr($contenedor,"<h3>Number of calls per tree : </h3>"))
    {    
        foreach($contenedor->find('td') as $li)
        {
            $numero_llamadas_arbol[$contNumLlamadasArbol] = $li;
            $contNumLlamadasArbol++;
        }
        //OK
    }

    //Contenedor - Actividad de Árbol
    else if ($arbol = strstr($contenedor,"<h3>Tree :"))
    {
        // Dato: Número del árbol (Tree : XXX)
        // Posiciones de inicio y final número de árbol        
        $arbol_no_tags = strip_tags($arbol);
        $var_inicio = strrpos(strip_tags($arbol_no_tags),":");
        $var_fin = strrpos(strip_tags($arbol_no_tags),"node");
        $dato = substr($arbol_no_tags, $var_inicio,$var_fin); 

        // Depuración de variable (Eliminación de caracteres NO numéricos)
        for($i = 0; $i < strlen($dato); $i++)
        {
            if(is_numeric($dato[$i]))
            {
                $num[$i] = $dato[$i];
            }
            else
            {
                $num[$i] = NULL;
            }
        }
        $var_test = implode("",$num);
        $cont = 0;
        $arboles[$cont] = $dato;  
        //OK      
        // Fin Extracción número árbol Principal

        // Inicio Extracción datos actividad por árbol
        foreach($contenedor->find('td') as $li)
        {
            $registro_actividad_arbol[$contActividadArbol] = $li;
            $contActividadArbol++;
        }
        //OK
        // Fin Extracción datos actividad por árbol
    }

    // Datos - Contenedor:  Tabla Causa Desconexión (cause / number of calls)
    else if (strstr($contenedor,"<h3>Disconnect cause : </h3>"))
    {
        foreach($contenedor->find('td') as $li)
        {
            $registro_causa_desconexion[$contCausaDesconexion] = $li;
            $contCausaDesconexion++;
        }
        // OK
    }
    
    // Datos - Contenedor:  Tabla Ubicación Desconexión (tree / node / count)
    else if (strstr($contenedor,"<h3>Location of Hangup : </h3>"))
    {
        foreach($contenedor->find('td') as $li)
        {
            $registro_ubicacion_desconexion[$contUbicacionDesconexion] = $li;
            $contUbicacionDesconexion++;
        }
        
    }
}
$sql2 = "DELETE FROM TB_INFORMACION_REP 
WHERE `INFO_REP_REPORTE_FECHA_INICIO`= '$fecha_inicio_reporte' AND 
    `INFO_REP_REPORTE_HORA_INICIO`= '$hora_inicio_reporte' AND 
    `INFO_REP_REPORTE_FECHA_FIN`= '$fecha_fin_reporte' AND 
    `INFO_REP_REPORTE_HORA_FIN`=     '$hora_fin_reporte' AND 
    `INFO_REP_DATOS_FECHA_INICIO`= '$fecha_inicio_datos' AND 
    `INFO_REP_DATOS_HORA_INICIO`= '$hora_inicio_datos' AND 
    `INFO_REP_NUMERO_REFERENCIA_INICIO`= '$numero_referencia_inicio' AND
    `INFO_REP_DATOS_FECHA_FIN`= '$fecha_fin_datos' AND
    `INFO_REP_DATOS_HORA_FIN`= '$hora_fin_datos' AND
    `INFO_REP_NUMERO_REFERENCIA_FIN`= '$numero_referencia_fin' AND
    `INFO_REP_NUMERO_TOTAL_LLAMADAS_TODOS_ARBOLES` = '$numero_total_llamadas_todos_arboles' ";
        //echo "!!!! " . $sql2 . "<br>";

$mysqli->query($sql2); 

$sql1 = "INSERT INTO 
TB_INFORMACION_REP 
(`INFO_REP_REPORTE_FECHA_INICIO`, 
    `INFO_REP_REPORTE_HORA_INICIO`, 
    `INFO_REP_REPORTE_FECHA_FIN`, 
    `INFO_REP_REPORTE_HORA_FIN`, 
    `INFO_REP_DATOS_FECHA_INICIO`, 
    `INFO_REP_DATOS_HORA_INICIO`, 
    `INFO_REP_NUMERO_REFERENCIA_INICIO`,
    `INFO_REP_DATOS_FECHA_FIN`,
    `INFO_REP_DATOS_HORA_FIN`,
    `INFO_REP_NUMERO_REFERENCIA_FIN`,
    `INFO_REP_NUMERO_TOTAL_LLAMADAS_TODOS_ARBOLES`)  

    VALUES 
    ('$fecha_inicio_reporte',
    '$hora_inicio_reporte',
    '$fecha_fin_reporte',
    '$hora_fin_reporte',
    '$fecha_inicio_datos',
    '$hora_inicio_datos',
    '$numero_referencia_inicio',
    '$fecha_fin_datos',
    '$hora_fin_datos',
    '$numero_referencia_fin',
    '$numero_total_llamadas_todos_arboles')";

    if ($mysqli->query($sql1) === TRUE) 
    {
       // echo "REGISTRO CREADO SATISFACTORIAMENTE: " . $sql1 . "<br>";
         header('Location: aareport.php');

    } 
    else  
    {
        echo "Error:" . $sql1 . "<br>" . $conn->error;
    }

    $k = 0;
    foreach ($numero_llamadas_arbol as $i)
    {
        $valor[$k] = $i;
        $k++;
        if ($k % 3 == 0 )
        {
            $valor_string = strip_tags("('" . implode( "','" , $valor) . "')");
            $sql2 = "INSERT INTO 
            TB_ARBOL 
            (ARBOL, 
            ARBOL_NUMERO_LLAMADAS, 
            ARBOL_DURACION_PROMEDIO)  

            VALUES 
            $valor_string";

            if ($mysqli->query($sql2) === TRUE) 
            {
                echo "REGISTRO CREADO SATISFACTORIAMENTE: " . $sql2 . "<br>";
            } 
            else  
            {
                echo "Error:" . $sql2 . "<br>" . $conn->error;
            }
            $valor = NULL;
        }
    }

    $k = 0;
    foreach ($registro_actividad_arbol as $j)
    {
        $valor[$k] = $j;
        $k++;
        if ($k % 5 == 0 )
        {
            $valor_string = strip_tags("('" . implode( "','" , $valor) . "')");
            $sql3 = "INSERT INTO 
            TB_ACTIVIDAD 
            (ACTIVIDAD_NODO, 
            ACTIVIDAD_CONTADOR_LLAMADAS,
            ACTIVIDAD_PORCENTAJE,
            ACTIVIDAD_NUM_LLAMADAS_SUSPENDIDAS,
            ACTIVIDAD_NUM_LLAMADAS_SUSPENDIDAS_POR_NODO)  

            VALUES 
            $valor_string";

            if ($mysqli->query($sql3) === TRUE) 
            {
                echo "REGISTRO CREADO SATISFACTORIAMENTE: " . $sql3 . "<br>";
            } 
            else  
            {
                echo "Error:" . $sql3 . "<br>" . $conn->error;
            }
            $valor = NULL;
        }
    }

    $k = 0;
    foreach ($registro_causa_desconexion as $l)
    {
        $valor[$k] = $l;
        $k++;
        if ($k % 2 == 0 )
        {
            $valor_string = strip_tags("('" . implode( "','" , $valor) . "')");
            $sql4 = "INSERT INTO 
            TB_DESCONEXION 
            (DESCONEXION_CAUSA, 
            DESCONEXION_NUMERO_LLAMADAS)  

            VALUES 
            $valor_string";

            if ($mysqli->query($sql4) === TRUE) 
            {
                echo "REGISTRO CREADO SATISFACTORIAMENTE: " . $sql4 . "<br>";
            } 
            else  
            {
                echo "Error:" . $sql4 . "<br>" . $conn->error;
            }
            $valor = NULL;
        }
    }

    $k = 0;
    foreach ($registro_ubicacion_desconexion as $m)
    {
        $valor[$k] = $m;
        $k++;
        if ($k % 3 == 0 )
        {
            $valor_string = strip_tags("('" . implode( "','" , $valor) . "')");
            $sql5 = "INSERT INTO 
            TB_SUSPENSION 
            (SUSPENSION_ARBOL, 
            SUSPENSION_NODO,
            SUSPENSION_CONTADOR_LLAMADAS)  

            VALUES 
            $valor_string";

            if ($mysqli->query($sql5) === TRUE) 
            {
                echo "REGISTRO CREADO SATISFACTORIAMENTE: " . $sql5 . "<br>";
            } 
            else  
            {
                echo "Error:" . $sql5 . "<br>" . $conn->error;
            }
            $valor = NULL;
        }
    }
    ?>